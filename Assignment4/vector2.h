#include <cmath>

template<typename T>
struct Vector2d
{
    T x;
    T y;

    // external operators
    // ------------------
    template<typename H, typename G, typename R>
    friend Vector2d<R> operator+(Vector2d<H> const & v1, Vector2d<G> const & v2);

    template<typename H, typename G, typename R>
    friend Vector2d<R> operator-(Vector2d<H> const & v1, Vector2d<G> const & v2);

    template<typename H, typename G, typename R>
    friend Vector2d<R> operator*(Vector2d<H> const & v, G const & c);

    template<typename H, typename G, typename R>
    friend Vector2d<R> operator*(G const & c, Vector2d<H> const & v);

    template<typename H, typename G, typename R>
    friend Vector2d<R> operator/(Vector2d<H> const & v1, G const & c);

    template<typename H, typename G, typename R>
    friend R dot(Vector2d<H> const & v1, Vector2d<G> const & v2);

    template<typename H, typename G>
    friend H length(Vector2d<H> const & v);

    template<typename H>
    friend H sqlength(Vector2d<H> const & v);

    template<typename H>
    friend Vector2d<H> normalize(Vector2d<H> const & v);

    template<typename H>
    friend Vector2d<H> rotate(Vector2d<H> const & v, double radians);

    template<typename H, typename G>
    friend bool operator==(Vector2d<H> const & v1, Vector2d<G> const & v2);

    template<typename H, typename G>
    friend bool operator!=(Vector2d<H> const & v1, Vector2d<G> const & v2);



    // SELF-operators
    // --------------
    template<typename G, typename R = decltype(T() * G())>
    R dot(Vector2d<G> const & v2) const
    {
        return static_cast<R>(x * v2.x + y * v2.y);
    }

    T sqlength() const
    {
        return dot(*this);
    }

    template<typename G=decltype(sqrt(T()))>
    G length() const
    {
        return sqrt( sqlength() ); 
    }

    template<typename G=decltype(sqrt(T()))>
    void normalize()
    {
        G len = length();
        
        if(len != static_cast<G>(1.0)) 
        {
            x /= len;
            y /= len;
        }        
    }

    template<typename G>
    Vector2d<T>& operator+=(Vector2d<G> const & v)
    {
        x += static_cast<T>(v.x);
        y += static_cast<T>(v.y);
        return *this;
    }

    template<typename G>
    Vector2d<T>& operator-=(Vector2d<G> const & v)
    {
        x -= static_cast<T>(v.x);
        y -= static_cast<T>(v.y);
        return *this;
    }

    template<typename G>
    Vector2d<T>& operator*=( G const & c)
    {
        x *= static_cast<T>(c);
        y *= static_cast<T>(c);
        return *this;
    }

    template<typename G>
    Vector2d<T>& operator/=( G const & c)
    {
        x /= static_cast<T>(c);
        y /= static_cast<T>(c);
        return *this;
    }


    void rotate(double radians)
    {
        double cos_value = cos(radians);
        if(std::abs(cos_value) < 1e-6)
        {
            cos_value = static_cast<T>(0.0);
        }

        double sin_value = sin(radians);
        if(std::abs(sin_value) < 1e-6)
        {
            sin_value = static_cast<T>(0.0);
        }
        
        T temp_x = static_cast<T>(cos_value * x - sin_value * y);
        
        y = static_cast<T>(sin_value * x + cos_value * y);
        x = temp_x;
    }


#ifndef ASSIGNMENT6
    // Streams
    // allow operator<< and operator>> to access private 
    // variables with friend declaration. This is not useful 
    // for a public struct, but will be for a more restricted
    // class.
    // In this case we have to use a different template name, because:
    // - using T would shadow the typename T declared at struct
    // - not using anything would result in "friend declares a non-template function"
    template<typename G>
    friend std::ostream& operator<<(std::ostream & o, Vector2d<G> const & v);
    template<typename G>
    friend std::istream& operator>>(std::istream & i, Vector2d<G> & v);
#endif
};

#ifndef ASSIGNMENT6
template<typename G>
std::ostream& operator<<(std::ostream & o, Vector2d<G> const & v)
{
    o << v.x << " " << v.y;
    return o;
}

template<typename G>
std::istream& operator>>(std::istream & i, Vector2d<G> & v)
{
    i >> v.x;
    i >> v.y;
    return i;
}
#endif

template<typename H, typename G, typename R = decltype(H() + G())>
Vector2d<R> operator+(Vector2d<H> const & v1, Vector2d<G> const & v2)
{
    return Vector2d<R>{v1.x + v2.x, v1.y + v2.y};
}

template<typename H, typename G, typename R = decltype(H() - G())>
Vector2d<R> operator-(Vector2d<H> const & v1, Vector2d<G> const & v2)
{
    return Vector2d<R>{v1.x - v2.x, v1.y - v2.y};
}

template<typename H, typename G, typename R = decltype(H() * G())>
Vector2d<R> operator*(Vector2d<H> const & v, G const & c)
{
    return Vector2d<R>{v.x * c, v.y * c};
}

// Scalar multiplication is not commutative e.g. if the underlying 
// ring is quaternions https://en.wikipedia.org/wiki/Quaternion
template<typename H, typename G, typename R = decltype(H() * G())>
Vector2d<R> operator*(G const & c, Vector2d<H> const & v)
{
    return Vector2d<R>{c * v.x, c * v.y};
}

template<typename H, typename G, typename R = decltype(H() / G())>
Vector2d<R> operator/(Vector2d<H> const & v1, G const & c)
{
    return Vector2d<R>{v1.x / c, v1.y / c };
}

template<typename H, typename G, typename R = decltype(H() * G())>
R dot(Vector2d<H> const & v1, Vector2d<G> const & v2)
{
    return v1.dot(v2); 
}

template<typename H, typename G = decltype(sqrt(H()))>
G length(Vector2d<H> const & v) 
{
    return v.length();
}

template<typename H>
H sqlength(Vector2d<H> const & v)
{
    return v.sqlength();
}

template<typename H>
Vector2d<H> normalize(Vector2d<H> const & v)
{
    Vector2d<H> res = v;
    res.normalize();
    return res;
}

template<typename H>
Vector2d<H> rotate(Vector2d<H> const & v, double radians)
{
    Vector2d<H> res = v;
    res.rotate(radians);
    return res;
}

template<typename H, typename G>
bool operator==(Vector2d<H> const & v1, Vector2d<G> const & v2)
{
    return (v1.x == v2.x && v1.y == v2.y);
}

template<typename H, typename G>
bool operator!=(Vector2d<H> const & v1, Vector2d<G> const & v2)
{
    return ! (v1 == v2);
}