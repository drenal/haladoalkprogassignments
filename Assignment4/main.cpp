#include <iostream>

#include "vector2.h"

static const double V1X = 2.3;
static const double V1Y = 5.8;
static const double V2X = 10.7;
static const double V2Y = 1.2;
static const int V3X = 2;
static const int V3Y = 5;
static const double C1 = 9.4;
static const int C2 = 3;

static const double pi = std::acos(-1);

using namespace std;

int main()
{
	auto err = [](auto str){ std::cout << "vector2.h error in: " << str << "\n"; std::exit(-1); };

    Vector2d<double> vd1{V1X, V1Y}, vd2{V2X, V2Y}, zerod{0.0,0.0};
    Vector2d<int> vi3{V3X, V3Y};

    double cd1 = C1;
    int ci2 = C2;

    // TEST#1: test equality
    cout << "Testing (in)equality of vectors...";
    if(vd1 != vd1)                                            { err("v1==v1 failed"); }
    if(vd1 == vd2)                                            { err("v1!=v2 failed"); }
    if(vi3 != vi3)                                            { err("v3==v3 failed"); }
    cout << " passed." << endl;

    // TEST#2: add two vectors
    {
        cout << "Testing adding two vectors...";
        Vector2d<double> res1{V1X+V2X, V1Y+V2Y};
        Vector2d<double> res2{V2X+V3X, V2Y+V3Y};
        Vector2d<double> temp{-V2X, -V2Y};

        if(vd1 + vd2 != res1)                                     { err("v1 + v2 == res1 failed"); }
        if(vd2 + vi3 != res2)                                     { err("v2 + v3 == res2 failed"); }
        temp += vd2;
        if(temp != zerod)                                        { err("(temp += v2) == zero failed"); }
        cout << " passed." << endl;
    }

    // TEST#3: subtract two vectors
    {
        cout << "Testing subtracting two vectors...";
        Vector2d<double> res1{V1X-V2X, V1Y-V2Y};
        Vector2d<double> res2{V2X-V3X, V2Y-V3Y};
        Vector2d<double> temp{V2X, V2Y};

        if(vd1 - vd2 != res1)                                     { err("v1 - v2 == res1 failed"); }
        if(vd2 - vi3 != res2)                                     { err("v2 - v3 == res2 failed"); }
        temp -= vd2;
        if(temp != zerod)                                        { err("(temp -= v2) == zero failed"); }
        cout << " passed." << endl;
    }

    // TEST#4: multiplying vector with scalar
    {
        cout << "Testing multiplication of a vector with scalar...";
        Vector2d<double> res1{V1X*C1, V1Y*C1};
        Vector2d<double> res2{V1X*C2, V1Y*C2};
        Vector2d<double> res3{V3X*C1, V3Y*C1};
        Vector2d<int> res4{V3X*C2, V3Y*C2};
        Vector2d<double> temp{V2X, V2Y};

        if(vd1 * cd1 != res1)                                     { err("v1 * c1 == res1 failed"); }
        if(cd1 * vd1 != res1)                                     { err("c1 * v1 == res1 failed"); }
        if(vd1 * ci2 != res2)                                     { err("v1 * c2 == res2 failed"); }
        if(ci2 * vd1 != res2)                                     { err("c2 * v1 == res2 failed"); }
        if(vi3 * cd1 != res3)                                     { err("v3 * c1 == res3 failed"); }
        if(cd1 * vi3 != res3)                                     { err("c1 * v3 == res3 failed"); }
        if(vi3 * ci2 != res4)                                     { err("v3 * c2 == res4 failed"); }
        if(ci2 * vi3 != res4)                                     { err("c2 * v3 == res5 failed"); }
        temp *= 0.0;
        if(temp != zerod)                                         { err("(temp *= 0) == zero failed"); }
        cout << " passed." << endl;
    }

    // TEST#5: dividing vector by const
    {
        cout << "Testing dividing vector by scalar...";
        Vector2d<double> res1{V1X/C1, V1Y/C1};
        Vector2d<double> res2{V1X/C2, V1Y/C2};
        Vector2d<double> res3{V3X/C1, V3Y/C1};
        Vector2d<int> res4{V3X/C2, V3Y/C2};
        Vector2d<double> temp{V2X, V2Y};

        if(vd1 / cd1 != res1)                                     { err("v1 / c1 == res1 failed"); }
        if(vd1 / ci2 != res2)                                     { err("v1 / c2 == res2 failed"); }
        if(vi3 / cd1 != res3)                                     { err("v3 / c1 == res3 failed"); }
        if(vi3 / ci2 != res4)                                     { err("v3 / c2 == res4 failed"); }
        temp /= 1.0;
        if(temp != vd2)                                           { err("(temp /= 1) == v2 failed"); }
        cout << " passed." << endl;
    }

    // TEST#6: dotproduct of two vectors
    {
        cout << "Testing dotproduct of two vectors...";
        double res1 = V1X*V2X + V1Y*V2Y;
        double res2 = V1X*V3X + V1Y*V3Y;
        double res3 = V3X*V3X + V3Y*V3Y;

        if(dot(vd1, vd2) != res1)                                 { err("v1 * v2 == res1 failed"); }
        if(dot(vi3, vd1) != res2)                                 { err("v3 * v1 == res2 failed"); }
        if(dot(vi3, vi3) != res3)                                 { err("v3 * v3 == res3 failed"); }
        if(vd1.dot(vd2) != res1)                                  { err("v1.dot(v2) == res1 failed"); }
        if(vi3.dot(vd1) != res2)                                  { err("v3.dot(v1) == res2 failed"); }
        if(vi3.dot(vi3) != res3)                                  { err("v3.dot(v3) == res3 failed"); }
        cout << " passed." << endl;
    }

    // TEST#7: length of vector
    {
        cout << "Testing length of vectors...";
        double res1 = sqrt(V1X*V1X + V1Y*V1Y);
        double res2 = sqrt(V3X*V3X + V3Y*V3Y);

        if(length(vd1) != res1)                                  { err("length(v1) == res1 failed"); }
        if(length(vi3) != res2)                                  { err("length(v3) == res2 failed"); }
        if(vd1.length() != res1)                                 { err("v1.length() == res1 failed"); }
        if(vi3.length() != res2)                                 { err("v3.length() == res2 failed"); }
        cout << " passed." << endl;

    }

    // TEST#8: sqlength of vector
    {
        cout << "Testing length squared of vectors...";
        double res1 = V1X*V1X + V1Y*V1Y;
        double res2 = V3X*V3X + V3Y*V3Y;

        if(sqlength(vd1) != res1)                                  { err("sqlength(v1) == res1 failed"); }
        if(sqlength(vi3) != res2)                                  { err("sqlength(v3) == res2 failed"); }
        if(vd1.sqlength() != res1)                                 { err("v1.sqlength() == res1 failed"); }
        if(vi3.sqlength() != res2)                                 { err("v3.sqlength() == res2 failed"); }
        cout << " passed." << endl;
    }


    // TEST#9: normalize vector
    {
        cout << "Testing normalization of vectors...";
        Vector2d<double> res1{V1X/sqrt(V1X*V1X + V1Y*V1Y), V1Y/sqrt(V1X*V1X + V1Y*V1Y)};
        Vector2d<int> res2{static_cast<int>(V3X/sqrt(V3X*V3X + V3Y*V3Y)), static_cast<int>(V3Y/sqrt(V3X*V3X + V3Y*V3Y))};
        Vector2d<double> temp1{V1X, V1Y};
        Vector2d<int> temp2{V3X, V3Y};

        if(normalize(vd1) != res1)                                  { err("normalize(v1) == res1 failed"); }
        if(normalize(vi3) != res2)                                  { err("normalize(v3) == res2 failed"); }
        temp1.normalize();
        if(temp1 != res1)                                           { err("v1.normalize() == res1 failed"); }
        temp2.normalize();
        if(temp2 != res2)                                           { err("v3.normalize() == res2 failed"); }
        cout << " passed." << endl;
    }

    // TEST#10: rotate vector
    {
        cout << "Testing rotation of vectors...";
        Vector2d<double> res1{0.0, 1.0};
        Vector2d<int> res2{0, 1};

        Vector2d<double> temp1{1.0, 0.0};
        Vector2d<int> temp2{1, 0};

        if(rotate(temp1, pi/2) != res1)                             { err("rotate(v1,90) == res1 failed"); }
        if(rotate(temp2, pi/2) != res2)                             { err("rotate(v3,90) == res2 failed"); }
        temp1.rotate(pi/2);
        if(temp1 != res1)                                           { err("v1.rotate((90) == res1 failed"); }
        temp2.rotate(pi/2);
        if(temp2 != res2)                                           { err("v3.rotate(90) == res2 failed"); }
        cout << " passed." << endl;
    }

    // TEST#11: read in vectors, print out vectors
    {
        cout << "Testing read in / write out... (when asked, write x value, press enter, write y value, press enter)";
        Vector2d<double> temp1{V1X, V1Y};
        Vector2d<double> temp2{V3X, V3Y};

        cout << "Enter vector 1:" << endl;
        cin >> temp1;
        cout << "Enter vector 2:" << endl;
        cin >> temp2;
        cout << "Vector 1 was (" << temp1 << "), vector 2 was (" << temp2 << ")" << endl;
    }


    cout << "All tests passed, nice job. :-)" << endl;

    return 0;
}
