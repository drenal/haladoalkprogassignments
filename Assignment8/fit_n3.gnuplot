f(x) = a*x**b
g(x) = c*x**d
fit f(x) "times_single_100.txt" via a,b
fit g(x) "times_thread_100.txt" via c,d

set title "Time of square matrix multiplication as a function of matrix dimension\nMinimum time is taken from 100 measurements for each dimension"
set xlabel "Dimension of square matrix (N)"
set ylabel "Minimum time of multiplication (t) [ms]"
set key left 

set term png size 800,800
set output "times.png"

plot "times_single_100.txt" using 1:2 t "single thread" ls 2, \
f(x) title sprintf("t(N)=%.10f * N^{%.10f}", a, b) ls 3, \
"times_thread_100.txt" using 1:2 t "multiple threads" ls 4, \
g(x) title sprintf("t(N)=%.10f * N^{%.10f}", c, d) ls 5
