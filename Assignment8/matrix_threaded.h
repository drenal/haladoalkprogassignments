#include <algorithm>
#include <vector>
#include <iostream>
#include <thread>
#include <future>

#include "../Assignment5/matrix.h"

template<typename T>
class MatrixThread
{
    // M - rows (indexed by i), N - columns (indexed by j), 
    // indexing begins always with 0
    int M, N;
    // data stores matrix in row-major manner
    std::vector<T> data;

	// position to index following row-major convention
	// first argument is row number, second argument column number
    int get_position(int i, int j) const
	{ 
		int row,col=0;

		if(i < 0) 
		{
			row = M + i;
		}
		else if(i >= M)
		{
			row = i - M;
		}
		else
		{
			row = i;			
		}

		if(j < 0) 
		{
			col = N + j;
		}
		else if(j >= N)
		{
			col = j - N;
		}
		else
		{
			col = j;
		}
		
		return N*row + col; 
	}

public:
    // Subscript operator for reading and writing:
	T &       operator()( int i, int j )       { return data.at(get_position(i,j)); }
	T const & operator()( int i, int j ) const { return data.at(get_position(i,j)); }

	//Function call operator for reading and writing:
	T &       operator[]( int i )       { return data.at(i); }
	T const & operator[]( int i ) const { return data.at(i); }

	// Default, Copy and Move constructors implemented by the compiler:
	MatrixThread() = default;

	MatrixThread( MatrixThread const& ) = default;

	// We have to define move constructor, so that self movement 
	MatrixThread( MatrixThread && ) = default;

	// Copy and Move assignment operators implemented by the compiler:
	MatrixThread<T>& operator=(MatrixThread<T> const&) = default;

	void copy_data_from(Matrix<T> const& mtx)
    {
        data = mtx.get_data();
        M = mtx.rows();
        N = mtx.columns();
    }

	MatrixThread<T>& operator=(MatrixThread<T> && mtx)
	{
		if(mtx.data != data) 
		{
			data = std::move(mtx.data);
			M = mtx.M;
			N = mtx.N;
		}
		return *this;
	}

	// Construct an empty MatrixThread with m rows and n columns
	MatrixThread(int m, int n) : M(m), N(n) 
	{
		data.resize(M*N);
	}

	// Construct from function by passing indices i from 0 to m-1 and j from 0 to n-1:
	template<typename F>
	MatrixThread(F f, int n, int m) : M(m), N(n)
	{
		data.resize(N*M);
		for(int i=0; i<m; ++i){ for(int j=0; j<n; ++j) {data[get_position(i,j)] = f(i, j); }}
	}
	
	// Construct from initializer list:
	MatrixThread( std::initializer_list<T> const& il, int n, int m ) : M(m), N(n), data{il}
	{
		int difference = data.size() - N * M;
		if(difference != 0)
		{
			data.resize(N*M, (T) 0.0);
		}
	}

	// TODO: nested initializer_list

	// ----------------------------------------	

	// Getter
	int rows() const
	{
		return M;
	}

	int columns() const
	{
		return N;
	}

	std::pair<int,int> get_position_from_index(int index) const
	{
		int row = (int) (index / M);
		int column = index - M * row;

		return std::pair<int,int>(row,column); 
	}
	
	std::vector<T> get_data() const
	{
		return data;
	}

	// Setter
	void set_data(std::initializer_list<T> const& il)
	{
		data.clear();
		data.insert(data.end(), il.begin(), il.end());
	}

	void set_dimension(int rows, int columns)
	{
		M = rows;
		N = columns;
		data.clear();
		data.resize(M*N);
	}

	// ----------------------------------------	

	// Add assignment operators:
	MatrixThread<T>& operator+= (MatrixThread<T> const& mtx)
	{
		std::transform(data.begin(), data.end(), mtx.cbegin(), data.begin(), [](T a, T b){return a+b;});
		return *this;
	}

	// Subtract assignment operators:
	MatrixThread<T>& operator-= (MatrixThread<T> const& mtx)
	{
		std::transform(data.begin(), data.end(), mtx.cbegin(), data.begin(), [](T a, T b){return a-b;});
		return *this;
	}

	// Multiplication by scalar:
	MatrixThread<T>& operator*= (T const& scl)
	{
		std::transform(data.begin(), data.end(), data.begin(), [scl](T a){return scl*a;});
		return *this;
	}

	// Division by scalar:
	MatrixThread<T>& operator/= (T const& scl)
	{
		std::transform(data.begin(), data.end(), data.begin(), [scl](T a){return a/scl;});
		return *this;
	}

	// Number of elements of the MatrixThread:
	int size() const
	{
		return static_cast<int>(data.size());
	}

	// begin and end for compatibility with STL:
	auto begin()
	{
		return data.begin();
	}

	auto cbegin() const
	{
		return data.cbegin();
	}

	auto end()
	{
		return data.end();
	}

	auto cend() const
	{
		return data.cend();
    }
};

// Addition operators 4 versions for all combinations of const& and &&:
template<typename T>
MatrixThread<T> operator+( MatrixThread<T> const & mtx1, MatrixThread<T> const & mtx2 )
{
	MatrixThread<T> result(mtx1.rows(), mtx1.columns());
	std::transform(mtx1.cbegin(), mtx1.cend(), mtx2.cbegin(), result.begin(), [](T a, T b){return a+b;});
	return result;
}

template<typename T>
MatrixThread<T>&& operator+( MatrixThread<T> && mtx1, MatrixThread<T> const & mtx2 )
{
	std::transform(mtx1.cbegin(), mtx1.cend(), mtx2.cbegin(), mtx1.begin(), [](T a, T b){return a+b;});
	return std::move(mtx1);
}

template<typename T>
MatrixThread<T>&& operator+( MatrixThread<T> const & mtx1, MatrixThread<T> && mtx2 )
{
	std::transform(mtx1.cbegin(), mtx1.cend(), mtx2.cbegin(), mtx2.begin(), [](T a, T b){return a+b;});
	return std::move(mtx2);
}

template<typename T>
MatrixThread<T>&& operator+( MatrixThread<T> && mtx1, MatrixThread<T> && mtx2 )
{
	std::transform(mtx1.cbegin(), mtx1.cend(), mtx2.cbegin(), mtx1.begin(), [](T a, T b){return a+b;});
	return std::move(mtx1);
}


// Subtraction operators 4 versions for all combinations of const& and &&:
template<typename T>
MatrixThread<T> operator-( MatrixThread<T> const & mtx1, MatrixThread<T> const & mtx2 )
{
	MatrixThread<T> result(mtx1.rows(), mtx1.columns());
	std::transform(mtx1.cbegin(), mtx1.cend(), mtx2.cbegin(), result.begin(), [](T a, T b){return a-b;});
	return result;
}

template<typename T>
MatrixThread<T>&& operator-( MatrixThread<T> && mtx1, MatrixThread<T> const & mtx2 )
{
	std::transform(mtx1.cbegin(), mtx1.cend(), mtx2.cbegin(), mtx1.begin(), [](T a, T b){return a-b;});
	return std::move(mtx1);
}

template<typename T>
MatrixThread<T>&& operator-( MatrixThread<T> const & mtx1, MatrixThread<T> && mtx2 )
{
	std::transform(mtx1.cbegin(), mtx1.cend(), mtx2.cbegin(), mtx2.begin(), [](T a, T b){return a-b;});
	return std::move(mtx2);
}

template<typename T>
MatrixThread<T>&& operator-( MatrixThread<T> && mtx1, MatrixThread<T> && mtx2 )
{
	std::transform(mtx1.cbegin(), mtx1.cend(), mtx2.cbegin(), mtx1.begin(), [](T a, T b){return a-b;});
	return std::move(mtx1);
}

// MatrixThread multiplication
// different, but matching (M_1 = N_2) MatrixThread dimensions should be handled
template<typename T>
MatrixThread<T> operator*( MatrixThread<T> const & mtx1, MatrixThread<T> const & mtx2)
{
	if(mtx1.columns() != mtx2.rows()) { std::cerr << "Error, MatrixThread multiplication was called with incompatible matrices" << std::endl; return mtx1; }

	MatrixThread<T> result(mtx1.rows(), mtx2.columns());

    auto multiplier = [&result, & mtx1, & mtx2](int i0, int i1){
        for(int i = i0; i < i1; ++i) 
        {
            for(int j=0; j<result.columns(); ++j)
            {
                for(int k=0; k<mtx1.columns(); ++k) 
                {
                    result[i*result.columns()+j] += mtx1(i,k) * mtx2(k,j);
                }
            }
        }
    };

    auto n = std::thread::hardware_concurrency();

    std::vector<std::future<void>> futures(n);

    for(unsigned int k=0; k<n; ++k )
    {
        auto i0 = k * result.rows() / n;
        auto i1 = k * result.rows() / n + result.rows() / n - 1;
        futures[k] = std::async( std::launch::async, multiplier, i0, i1 );
    }

    for(unsigned int k=0; k<n; ++k)
    {
        futures[k].wait();
    }

	return result;
}

// Scalar multiplication
template<typename T>
MatrixThread<T> operator*(MatrixThread<T> const& mtx, T const& scl)
{
	MatrixThread<T> result(mtx.rows(), mtx.columns()); 
	std::transform(mtx.cbegin(), mtx.cend(), result.begin(), [scl](T a){return a*scl;});
	return result;
}

template<typename T>
MatrixThread<T>&& operator*(MatrixThread<T>&& mtx, T const& scl)
{
	std::transform(mtx.cbegin(), mtx.cend(), mtx.begin(), [scl](T a){return a*scl;});
	return std::move(mtx);
}

template<typename T>
MatrixThread<T> operator*(T const& scl, MatrixThread<T> const& mtx)
{
	MatrixThread<T> result(mtx.rows(), mtx.columns()); 
	std::transform(mtx.cbegin(), mtx.cend(), result.begin(), [scl](T a){return scl*a;});
	return result;
}

template<typename T>
MatrixThread<T>&& operator*(T const& scl, MatrixThread<T>&& mtx)
{
	std::transform(mtx.cbegin(), mtx.cend(), mtx.begin(), [scl](T a){return scl*a;});
	return std::move(mtx);
}


// Scalar division
template<typename T>
MatrixThread<T> operator/(MatrixThread<T> const& mtx, T const& scl)
{
	MatrixThread<T> result(mtx.rows(), mtx.columns());
	std::transform(mtx.cbegin(), mtx.cend(), result.begin(), [scl](T a){return a / scl;});
	return result;
}

template<typename T>
MatrixThread<T>&& operator/(MatrixThread<T>&& mtx, T const& scl)
{
	std::transform(mtx.cbegin(), mtx.cend(), mtx.begin(), [scl](T a){return a / scl;});
	return std::move(mtx);
}


#ifndef ASSIGNMENT6
// ostream <<
template<typename T>
std::ostream& operator<< (std::ostream & o, MatrixThread<T> const & mtx)
{
	for(int i=0; i<mtx.rows(); ++i)
	{
		for(int j=0; j<mtx.columns(); ++j)
		{
			T temp = mtx(i,j);
			o << temp << "   ";
		}
		o << std::endl;
	}
	return o;
}
#endif

template<typename H, typename G>
bool operator==(MatrixThread<H> const & mtx1, MatrixThread<G> const & mtx2)
{
	if(mtx1.rows() != mtx2.rows() || mtx1.columns() != mtx2.columns()) { return false; }

	return std::equal(mtx1.cbegin(), mtx1.cend(), mtx1.cbegin(), [](H a, G b){return a == b; });
}

template<typename H, typename G>
bool operator!=(MatrixThread<H> const & mtx1, MatrixThread<G> const & mtx2)
{
    return ! (mtx1 == mtx2);
}