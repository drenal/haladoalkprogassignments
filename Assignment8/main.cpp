#include <random>
#include <iostream>
#include <fstream>
#include <iterator>
#include <chrono>
#include <map>
#include <sstream>

#include "matrix_threaded.h"

// Don't forget to build with Release mode:
// cmake -DCMAKE_BUILD_TYPE=Release ..
// make
// ./matrix_threads

using namespace std;

template<typename T>
vector<T> linspace(T start, T end, int elements)
{
    vector<T> result(elements);
    T step = ( end - start ) / static_cast<T>(elements-1);
    T j = start;

    generate(result.begin(), result.end(), [j,step]() mutable {j += step; return j-step;});

    return result;
}

int main()
{
    random_device rd{};
    mt19937 gen(rd());
    normal_distribution<double> distr(100,20);

    // get x axis values
    vector<double> range = linspace(10.0, 1000.0, 100);

    map<double,pair<double,double>> results;
    // fill out the map's keys with the x axis values, set y value to zero
    std::transform(range.cbegin(), range.cend(), inserter(results, results.begin()), [](double value){
        return pair<double, pair<double,double>>(value, pair<double,double>(0.0,0.0));
    });

    for(auto it = results.begin(); it != results.end(); ++it)
    {
        int loops = 100;
        vector<double> times_measured_single(loops);
        vector<double> times_measured_thread(loops);

        for(int i = 0; i < loops; ++i)
        {
            Matrix<double> mtxs1(it->first, it->first);
            generate(mtxs1.begin(), mtxs1.end(), [&]{return distr(gen); });
            Matrix<double> mtxs2(it->first, it->first);
            generate(mtxs2.begin(), mtxs2.end(), [&]{return distr(gen); });

            MatrixThread<double> mtxt1;
            mtxt1.copy_data_from(mtxs1);
            MatrixThread<double> mtxt2;
            mtxt2.copy_data_from(mtxs2);

            auto t1 = chrono::high_resolution_clock::now();
            Matrix<double> mtxs3 = mtxs1 * mtxs2;
            auto t2 = chrono::high_resolution_clock::now();

            auto t3 = chrono::high_resolution_clock::now();
            MatrixThread<double> mtxt3 = mtxt1 * mtxt2;
            auto t4 = chrono::high_resolution_clock::now();

            times_measured_single.at(i) = (static_cast<std::chrono::duration<double, std::milli>>(t2-t1)).count();
            times_measured_thread.at(i) = (static_cast<std::chrono::duration<double, std::milli>>(t4-t3)).count();

            int matching = 0;
            for(int i=0; i< it->first; ++i)
            {
                if(std::abs(mtxs3[i] - mtxt3[i]) > 1e-16)
                {
                    matching++;
                }
            }
            if(matching > 0)
            {
                std::cout << "Warning: the results of single thread and multiple threads multiplications do not match at " << matching << " places." << std::endl;
                std::cout << "Matrix size " << it->first << std::endl;
            }

        }

        // get minimum
        it->second.first  = *(min_element(times_measured_single.begin(), times_measured_single.end()));
        it->second.second = *(min_element(times_measured_thread.begin(), times_measured_thread.end()));
    }

    // convert results map to a string vector
    vector<string> results_string_single;
    vector<string> results_string_thread;
    std::transform(results.cbegin(), results.cend(), back_inserter(results_string_single), [](pair<double,pair<double,double>> const & value){
        stringstream ss; 
        ss << value.first << " " << value.second.first; 
        return ss.str();
    } );
    std::transform(results.cbegin(), results.cend(), back_inserter(results_string_thread), [](pair<double,pair<double,double>> const & value){
        stringstream ss; 
        ss << value.first << " " << value.second.second; 
        return ss.str();
    } );

    // write out to file
    std::ofstream output("times_single.txt");
    if(output.is_open())
    {
        copy(results_string_single.begin(), results_string_single.end(), ostream_iterator<string>(output, "\n") );
    }

    std::ofstream output2("times_thread.txt");
    if(output2.is_open())
    {
        copy(results_string_thread.begin(), results_string_thread.end(), ostream_iterator<string>(output2, "\n") );
    }

    return 0;
}