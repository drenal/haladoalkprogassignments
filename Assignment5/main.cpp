#include "matrix.h"
#include <iostream>

static const double M1 = 3.1;
static const double M2 = 5.2;
static const double M3 = 9.3;
static const double M4 = 7.1;
static const double M5 = 15.2;
static const double M6 = 11.3;

using namespace std; 


int main()
{
	auto err = [](auto str){ std::cout << "matrix.h error in: " << str << "\n"; std::exit(-1); };

	// Test default constructor:
	{
		Matrix<double> u;
		if(u.size() != 0)         { err("default contructor test [size]");           }
		if(u.begin() != u.end())  { err("default contructor test [begin == end]");   }
		if(u.cbegin() != u.cend()){ err("default contructor test [cbegin == cend]"); }
	}

	// Test list initialization and indexing:
	{
		Matrix<double> a({M1, M2, M3, M4, M5, M6}, 3, 2);
		if(a.size() != 6)                                   {err("initializer list constructor test [size]");               }
		if(a[0] != M1 || a[1] != M2 || a[2] != M3 || a[3] != M4 || a[4] != M5 || a[5] != M6 )          { err("initializer list constructor test [indexing with []]");   }
		if(a(0,0) != M1 || a(0,1) != M2 || a(0,2) != M3 || a(1,0) != M4 || a(1,1) != M5 || a(1,2) != M6 )    { err("initializer list constructor test [indexing with ()]");   }
		if( ++ ++ ++ ++ ++ ++(a.begin()) != a.end() )                { err("initializer list constructor test [begin + 6 == end]");   }
		if( ++ ++ ++ ++ ++ ++(a.cbegin()) != a.cend() )              { err("initializer list constructor test [cbegin + 6 == cend]"); }
	}

	//Test copy constructor:
	{
		Matrix<double> a({M1, M2, M3, M4, M5, M6}, 3, 2);
		Matrix<double> b{a};
		if(b.size() != 6)                                { err("copy constructor test [size]");               }
		if(b[0] != M1 || b[1] != M2 || b[2] != M3 || b[3] != M4 || b[4] != M5 || b[5] != M6 )       { err("copy constructor test [elements]");           }
		if( ++ ++ ++ ++ ++ ++(b.begin()) != b.end() )             { err("copy constructor test [begin + 6 == end]");   }
		if( ++ ++ ++ ++ ++ ++(b.cbegin()) != b.cend() )           { err("copy constructor test [cbegin + 6 == cend]"); }
		if(a.size() != 6)                                { err("copy constructor test [src size]");           }
		if(a[0] != M1 || a[1] != M2 || a[2] != M3 || a[3] != M4 || a[4] != M5 || a[5] != M6 )       { err("copy constructor test [src elements]");       }
	}

	//Test move constructor:
	{
		Matrix<double> a({M1, M2, M3, M4, M5, M6}, 2,  3);
		Matrix<double> b{ std::move(a) };
		if(a.size() != 0)                                { err("move constructor test [src size]");             }
		if(a.begin() != a.end())                         { err("move constructor test [src begin == src end]"); }
		if(b.size() != 6)                                { err("move constructor test [size]");                 }
		if(b[0] != M1 || b[1] != M2 || b[2] != M3 || b[3] != M4 || b[4] != M5 || b[5] != M6 )       { err("move constructor test [elements]");             }
		if( ++ ++ ++ ++ ++ ++(b.begin()) != b.end() )             { err("move constructor test [begin + 6 == end]");     }
		if( ++ ++ ++ ++ ++ ++(b.cbegin()) != b.cend() )           { err("move constructor test [cbegin + 6 == cend]");   }
	}
	
	//Test indexible function constructor:
	{
		Matrix<int> a{ [](int i, int j){ return i==j ? 1 : 0; }, 3, 3};
		if(a.size() != 9)                                                 { err("constructor from indexible test [size]");     }
		if(a[0] != 1 || a[2] != 0 || a[4] != 1 || a[3] != 0 || a[8] != 1) { err("constructor from indexible test [elements]"); }
	}

	//Test assignment:
	{
		Matrix<double> a{ [](int i, int j){ return i==j ? 1.0 : 0.0; }, 3, 3};
		Matrix<double> b;
		b = a;
		if(b.size() != 9)                            { err("assignment test [size]");         }
		if(a[0] != 1.0 || a[2] != 0.0 || a[4] != 1.0 || a[3] != 0.0 || a[8] != 1.0){ err("assignment test [elements]");     }
		if(a.size() != 9)                            { err("assignment test [src size]");     }
		if(a[0] != 1.0 || a[2] != 0.0 || a[4] != 1.0 || a[3] != 0.0 || a[8] != 1.0){ err("assignment test [src elements]"); }
	}

	//Test self assignment:
	{
		Matrix<double> c{ [](int i, int j){ return i==j ? 1.0 : 0.0; }, 3, 3};
		c = c;
		if(c.size() != 9)                            { err("self assignment test [size]");     }
		if(c[0] != 1.0 || c[2] != 0.0 || c[4] != 1.0 || c[3] != 0.0 || c[8] != 1.0){ err("self assignment test [elements]"); }
	}

	//Test move assignment:
	{
		Matrix<double> a{ [](int i, int j){ return i==j ? 1.0 : 0.0; }, 3, 3};
		Matrix<double> b;
		b = std::move(a);
		if(a.begin() != a.end())                     { err("assignment test [src begin == src end]"); }
		if(a.size() != 0)                            { err("assignment test [src size]");             }
		if(b.size() != 9)                            { err("assignment test [size]");                 }
		if(b[0] != 1.0 || b[2] != 0.0 || b[4] != 1.0 || b[3] != 0.0 || b[8] != 1.0){ err("assignment test [elements]");             }
	}

	//Test self move assignment:
	{
		Matrix<double> c({M1, M2, M3, M4, M5, M6}, 2,  3);
		c = std::move(c);
		if(c.size() != 6)                            { err("self assignment test [size]");     }
		if(c[0] != M1 || c[1] != M2 || c[2] != M3 || c[3] != M4 || c[4] != M5 || c[5] != M6)    { err("self assignment test [elements]"); }
	}

	//Test operator- (l-value, l-value) (more detailed test, since later tests depend on it)
	{
		Matrix<double> a({M1, M2, M3, M4, M5, M6}, 3, 2);
		Matrix<double> b({M4, M5, M6, M1, M2, M3}, 3, 2);
		Matrix<double> ref({M4-M1, M5-M2, M6-M3, M1-M4, M2-M5, M3-M6}, 3, 2);
		Matrix<double> res = b - a;
		if(a.size() != 6)                              { err("operator- test (l-value, l-value) [src size]");     }
		if(a[0] != M1 || a[1] != M2 || a[2] != M3 || a[3] != M4 || a[4] != M5 || a[5] != M6 )     { err("operator- test (l-value, l-value) [src elements]"); }
		if(b.size() != 6)                              { err("operator- test (l-value, l-value) [src size]");     }
		if(b[0] != M4 || b[1] != M5 || b[2] != M6 || b[3] != M1 || b[4] != M2 || b[5] != M3 )     { err("operator- test (l-value, l-value) [src elements]"); }
		if(res.size() != 6)                            { err("operator- test (l-value, l-value) [size]");         }
		if(std::abs(ref[0]-res[0]) > 1e-15 ||
		   std::abs(ref[1]-res[1]) > 1e-15 || 
		   std::abs(ref[3]-res[3]) > 1e-15 || 
		   std::abs(ref[4]-res[4]) > 1e-15 || 
		   std::abs(ref[5]-res[5]) > 1e-15 || 
		   std::abs(ref[2]-res[2]) > 1e-15    ){ err("operator- test (l-value, l-value) [elements]"); }
	}

	//Test operator- (r-value, l-value)
	{
		Matrix<double> a({M1, M2, M3, M4, M5, M6}, 3, 2);
		Matrix<double> b({M4, M5, M6, M1, M2, M3}, 3, 2);
		Matrix<double> ref({M4-M1, M5-M2, M6-M3, M1-M4, M2-M5, M3-M6}, 3, 2);
		Matrix<double> res = std::move(b) - a;
		if(a.size() != 6)                              { err("operator- test (r-value, l-value) [src size]");     }
		if(b.size() != 0)                              { err("operator- test (r-value, l-value) [src size]");     }
		if(res.size() != 6)                            { err("operator- test (r-value, l-value) [size]");         }
		if(a[0] != M1 || a[1] !=  M2 || a[2] !=  M3 || a[3] != M4 || a[4] != M5 || a[5] != M6 )   { err("operator- test (r-value, l-value) [src elements]"); }
		if(std::abs(ref[0]-res[0]) > 1e-15 ||
		   std::abs(ref[1]-res[1]) > 1e-15 || 
		   std::abs(ref[3]-res[3]) > 1e-15 || 
		   std::abs(ref[4]-res[4]) > 1e-15 || 
		   std::abs(ref[5]-res[5]) > 1e-15 || 
		   std::abs(ref[2]-res[2]) > 1e-15    )        { err("operator- test (r-value, l-value) [elements]"); }
	}

	//Test operator- (l-value, r-value)
	{
		Matrix<double> a({M1, M2, M3, M4, M5, M6}, 3, 2);
		Matrix<double> b({M4, M5, M6, M1, M2, M3}, 3, 2);
		Matrix<double> ref({M4-M1, M5-M2, M6-M3, M1-M4, M2-M5, M3-M6}, 3, 2);
		Matrix<double> res = b - std::move(a);
		if(a.size() != 0)                              { err("operator- test (l-value, r-value) [src size]");     }
		if(b.size() != 6)                              { err("operator- test (l-value, r-value) [src size]");     }
		if(res.size() != 6)                            { err("operator- test (l-value, r-value) [size]");         }
		if(b[0] != M4 || b[1] != M5 || b[2] != M6 || b[3] != M1 || b[4] != M2 || b[5] != M3 )     { err("operator- test (l-value, r-value) [src elements]"); }
		if(std::abs(ref[0]-res[0]) > 1e-15 ||
		   std::abs(ref[1]-res[1]) > 1e-15 || 
		   std::abs(ref[3]-res[3]) > 1e-15 || 
		   std::abs(ref[4]-res[4]) > 1e-15 || 
		   std::abs(ref[5]-res[5]) > 1e-15 || 
		   std::abs(ref[2]-res[2]) > 1e-15    )        { err("operator- test (l-value, r-value) [elements]"); }
	}

	//Test operator- (r-value, r-value)
	{
		Matrix<double> a({M1, M2, M3, M4, M5, M6}, 3, 2);
		Matrix<double> b({M4, M5, M6, M1, M2, M3}, 3, 2);
		Matrix<double> ref({M4-M1, M5-M2, M6-M3, M1-M4, M2-M5, M3-M6}, 3, 2);
		Matrix<double> res = std::move(b) - std::move(a);
		if(a.size() != 6)            { err("operator- test (r-value, r-value) [src size]"); }//this argument was not reused!
		if(b.size() != 0)            { err("operator- test (r-value, r-value) [src size]"); }
		if(res.size() != 6)          { err("operator- test (r-value, r-value) [size]");     }
		if(std::abs(ref[0]-res[0]) > 1e-15 ||
		   std::abs(ref[1]-res[1]) > 1e-15 || 
		   std::abs(ref[3]-res[3]) > 1e-15 || 
		   std::abs(ref[4]-res[4]) > 1e-15 || 
		   std::abs(ref[5]-res[5]) > 1e-15 || 
		   std::abs(ref[2]-res[2]) > 1e-15    )        { err("operator- test (r-value, r-value) [elements]"); }
	}

	//Test operator+ (l-value, l-value)
	{
		Matrix<double> a({M1, M2, M3, M4, M5, M6}, 3, 2);
		Matrix<double> b({M4, M5, M6, M1, M2, M3}, 3, 2);
		Matrix<double> ref({M4+M1, M5+M2, M6+M3, M1+M4, M2+M5, M3+M6}, 3, 2);
		Matrix<double> res = a + b;
		if(a.size() != 6)                              { err("operator+ test (l-value, l-value) [src size]");     }
		if(b.size() != 6)                              { err("operator+ test (l-value, l-value) [src size]");     }
		if(res.size() != 6)                            { err("operator+ test (l-value, l-value) [size]");         }
		if(a[0] != M1 || a[1] !=  M2 || a[2] != M3 || a[3] != M4 || a[4] != M5 || a[5] != M6 )    { err("operator+ test (l-value, l-value) [src elements]"); }
		if(b[0] != M4 || b[1] != M5 || b[2] != M6 || b[3] != M1 || b[4] != M2 || b[5] != M3 )     { err("operator+ test (l-value, l-value) [src elements]"); }
		if(std::abs(ref[0]-res[0]) > 1e-15 ||
		   std::abs(ref[1]-res[1]) > 1e-15 || 
		   std::abs(ref[3]-res[3]) > 1e-15 || 
		   std::abs(ref[4]-res[4]) > 1e-15 || 
		   std::abs(ref[5]-res[5]) > 1e-15 || 
		   std::abs(ref[2]-res[2]) > 1e-15    )        { err("operator+ test (l-value, l-value) [elements]"); }
	}

	//Test operator+ (r-value, l-value)
	{
		Matrix<double> a({M1, M2, M3, M4, M5, M6}, 3, 2);
		Matrix<double> b({M4, M5, M6, M1, M2, M3}, 3, 2);
		Matrix<double> ref({M4+M1, M5+M2, M6+M3, M1+M4, M2+M5, M3+M6}, 3, 2);
		Matrix<double> res = std::move(a) + b;
		if(a.size() != 0)                              { err("operator+ test (r-value, l-value) [src size]");     }
		if(b.size() != 6)                              { err("operator+ test (r-value, l-value) [src size]");     }
		if(res.size() != 6)                            { err("operator+ test (r-value, l-value) [size]");         }
		if(b[0] != M4 || b[1] != M5 || b[2] != M6 || b[3] != M1 || b[4] != M2 || b[5] != M3 )     { err("operator+ test (r-value, l-value) [src elements]"); }
		if(std::abs(ref[0]-res[0]) > 1e-15 ||
		   std::abs(ref[1]-res[1]) > 1e-15 || 
		   std::abs(ref[3]-res[3]) > 1e-15 || 
		   std::abs(ref[4]-res[4]) > 1e-15 || 
		   std::abs(ref[5]-res[5]) > 1e-15 || 
		   std::abs(ref[2]-res[2]) > 1e-15    )        { err("operator+ test (r-value, l-value) [elements]"); }
	}

	//Test operator+ (l-value, r-value)
	{
		Matrix<double> a({M1, M2, M3, M4, M5, M6}, 3, 2);
		Matrix<double> b({M4, M5, M6, M1, M2, M3}, 3, 2);
		Matrix<double> ref({M4+M1, M5+M2, M6+M3, M1+M4, M2+M5, M3+M6}, 3, 2);
		Matrix<double> res = a + std::move(b);
		if(a.size() != 6)                              { err("operator+ test (l-value, r-value) [src size]");     }
		if(b.size() != 0)                              { err("operator+ test (l-value, r-value) [src size]");     }
		if(res.size() != 6)                            { err("operator+ test (l-value, r-value) [size]");         }
		if(a[0] != M1 || a[1] !=  M2 || a[2] !=  M3 || a[3] != M4 || a[4] != M5 || a[5] != M6 ){ err("operator+ test (l-value, r-value) [src elements]"); }
		if(std::abs(ref[0]-res[0]) > 1e-15 ||
		   std::abs(ref[1]-res[1]) > 1e-15 ||
		   std::abs(ref[3]-res[3]) > 1e-15 || 
		   std::abs(ref[4]-res[4]) > 1e-15 || 
		   std::abs(ref[5]-res[5]) > 1e-15 ||  
		   std::abs(ref[2]-res[2]) > 1e-15    )        { err("operator+ test (l-value, r-value) [elements]"); }
	}

	//Test operator+ (r-value, r-value)
	{
		Matrix<double> a({M1, M2, M3, M4, M5, M6}, 3, 2);
		Matrix<double> b({M4, M5, M6, M1, M2, M3}, 3, 2);
		Matrix<double> ref({M4+M1, M5+M2, M6+M3, M1+M4, M2+M5, M3+M6}, 3, 2);
		Matrix<double> res = std::move(a) + std::move(b);
		if(a.size() != 0)            { err("operator+ test (r-value, r-value) [src size]"); }
		if(b.size() != 6)            { err("operator+ test (r-value, r-value) [src size]"); }//this argument was not reused!
		if(res.size() != 6)          { err("operator+ test (r-value, r-value) [size]");     }
		if(std::abs(ref[0]-res[0]) > 1e-15 ||
		   std::abs(ref[1]-res[1]) > 1e-15 || 
		   std::abs(ref[3]-res[3]) > 1e-15 || 
		   std::abs(ref[4]-res[4]) > 1e-15 || 
		   std::abs(ref[5]-res[5]) > 1e-15 || 
		   std::abs(ref[2]-res[2]) > 1e-15    )        { err("operator+ test (r-value, r-value) [elements]"); }
	}

	//Test +=:
	{
		Matrix<double> a({M1, M2, M3, M4, M5, M6}, 3, 2);
		Matrix<double> b({M4, M5, M6, M1, M2, M3}, 3, 2);
		Matrix<double> ref({M4+M1, M5+M2, M6+M3, M1+M4, M2+M5, M3+M6}, 3, 2);
	    a += b;
		if(a.size() != 6)                              { err("+= test [size]");         }
		if(b.size() != 6)                              { err("+= test [src size]");     }
		if(b[0] != M4 || b[1] != M5 || b[2] != M6 || b[3] != M1 || b[4] != M2 || b[5] != M3 ){ err("+= test [src elements]"); }
		if(std::abs(ref[0]-a[0]) > 1e-15 ||
		   std::abs(ref[1]-a[1]) > 1e-15 || 
		   std::abs(ref[3]-a[3]) > 1e-15 || 
		   std::abs(ref[4]-a[4]) > 1e-15 || 
		   std::abs(ref[5]-a[5]) > 1e-15 || 
		   std::abs(ref[2]-a[2]) > 1e-15    )        { err("+= test [elements]"); }
		
	}

	//Test -=:
	{
		Matrix<double> a({M1, M2, M3, M4, M5, M6}, 3, 2);
		Matrix<double> b({M4, M5, M6, M1, M2, M3}, 3, 2);
		Matrix<double> ref({M4-M1, M5-M2, M6-M3, M1-M4, M2-M5, M3-M6}, 3, 2);
		b -= a;
		if(a.size() != 6)                            { err("-= test [size]");         }
		if(b.size() != 6)                            { err("-= test [src size]");     }
		if(a[0] != M1 || a[1] != M2 || a[2] != M3 || a[3] != M4 || a[4] != M5 || a[5] != M6 ){ err("-= test [src elements]"); }
		if(std::abs(ref[0]-b[0]) > 1e-15 ||
		   std::abs(ref[1]-b[1]) > 1e-15 || 
		   std::abs(ref[3]-b[3]) > 1e-15 || 
		   std::abs(ref[4]-b[4]) > 1e-15 || 
		   std::abs(ref[5]-b[5]) > 1e-15 || 
		   std::abs(ref[2]-b[2]) > 1e-15    )        { err("-= test [elements]"); }
	}

	//Test *=:
	{
		Matrix<double> a({M1, M2, M3, M4, M5, M6}, 3, 2);
		Matrix<double> ref({2.0*M1, 2.0*M2, 2.0*M3, 2.0*M4, 2.0*M5, 2.0*M6}, 3, 2);
		a *= 2.0;
		if(a.size() != 6)          { err("*= test [size]");  }
		if(std::abs(ref[0]-a[0]) > 1e-15 ||
		   std::abs(ref[1]-a[1]) > 1e-15 || 
		   std::abs(ref[3]-a[3]) > 1e-15 || 
		   std::abs(ref[4]-a[4]) > 1e-15 || 
		   std::abs(ref[5]-a[5]) > 1e-15 || 
		   std::abs(ref[2]-a[2]) > 1e-15    )        { err("*= test [elements]"); }
	}

	//Test operator* (l-value, scalar)
	{
		Matrix<double> a({M1, M2, M3, M4, M5, M6}, 3, 2);
		Matrix<double> ref({2.0*M1, 2.0*M2, 2.0*M3, 2.0*M4, 2.0*M5, 2.0*M6}, 3, 2);
		Matrix<double> res = a * 2.0;
		if(a.size()   != 6)                           { err("operator* test (l-value, scalar) [src size]");     }
		if(res.size() != 6)                           { err("operator* test (l-value, scalar) [size]");         }
		if(a[0] != M1 || a[1] != M2 || a[2] != M3 || a[3] != M4 || a[4] != M5 || a[5] != M6 ){ err("operator* test (l-value, scalar) [src elements]"); }
		if(std::abs(ref[0]-res[0]) > 1e-15 ||
		   std::abs(ref[1]-res[1]) > 1e-15 || 
		   std::abs(ref[3]-res[3]) > 1e-15 || 
		   std::abs(ref[4]-res[4]) > 1e-15 || 
		   std::abs(ref[5]-res[5]) > 1e-15 || 
		   std::abs(ref[2]-res[2]) > 1e-15    )        { err("operator* test (l-value, scalar) [elements]"); }
	}

	//Test operator* (r-value, scalar)
	{
		Matrix<double> a({M1, M2, M3, M4, M5, M6}, 3, 2);
		Matrix<double> ref({2.0*M1, 2.0*M2, 2.0*M3, 2.0*M4, 2.0*M5, 2.0*M6}, 3, 2);
		Matrix<double> res = std::move(a) * 2.0;
		if(a.size()   != 0)          { err("operator* test (r-value, scalar) [src size]");     }
		if(res.size() != 6)          { err("operator* test (r-value, scalar) [size]");         }
		if(std::abs(ref[0]-res[0]) > 1e-15 ||
		   std::abs(ref[1]-res[1]) > 1e-15 || 
		   std::abs(ref[3]-res[3]) > 1e-15 || 
		   std::abs(ref[4]-res[4]) > 1e-15 || 
		   std::abs(ref[5]-res[5]) > 1e-15 || 
		   std::abs(ref[2]-res[2]) > 1e-15    )        { err("operator* test (r-value, scalar) [elements]"); }
	}

	//Test operator* (scalar, l-value)
	{
		Matrix<double> a({M1, M2, M3, M4, M5, M6}, 3, 2);
		Matrix<double> ref({2.0*M1, 2.0*M2, 2.0*M3, 2.0*M4, 2.0*M5, 2.0*M6}, 3, 2);
		Matrix<double> res = 2.0 * a;
		if(a.size()   != 6)                           { err("operator* test (scalar, l-value) [src size]");     }
		if(res.size() != 6)                           { err("operator* test (scalar, l-value) [size]");         }
		if(a[0] != M1 || a[1] != M2 || a[2] != M3 || a[3] != M4 || a[4] != M5 || a[5] != M6 ){ err("operator* test (scalar, l-value) [src elements]"); }
		if(std::abs(ref[0]-res[0]) > 1e-15 ||
		   std::abs(ref[1]-res[1]) > 1e-15 || 
		   std::abs(ref[3]-res[3]) > 1e-15 || 
		   std::abs(ref[4]-res[4]) > 1e-15 || 
		   std::abs(ref[5]-res[5]) > 1e-15 || 
		   std::abs(ref[2]-res[2]) > 1e-15    )        { err("operator* test (scalar, l-value) [elements]"); }
	}

	//Test operator* (scalar, r-value)
	{
		Matrix<double> a({M1, M2, M3, M4, M5, M6}, 3, 2);
		Matrix<double> ref({2.0*M1, 2.0*M2, 2.0*M3, 2.0*M4, 2.0*M5, 2.0*M6}, 3, 2);
		Matrix<double> res = 2.0 * std::move(a);
		if(a.size()   != 0)          { err("operator* test (scalar, r-value) [src size]");     }
		if(res.size() != 6)          { err("operator* test (scalar, r-value) [size]");         }
		if(std::abs(ref[0]-res[0]) > 1e-15 ||
		   std::abs(ref[1]-res[1]) > 1e-15 || 
		   std::abs(ref[3]-res[3]) > 1e-15 || 
		   std::abs(ref[4]-res[4]) > 1e-15 || 
		   std::abs(ref[5]-res[5]) > 1e-15 || 
		   std::abs(ref[2]-res[2]) > 1e-15    )        { err("operator* test (scalar, r-value) [elements]"); }
	}

	//Test /=:
	{
		Matrix<double> a({M1, M2, M3, M4, M5, M6}, 3, 2);
		Matrix<double> ref({M1/2.0, M2/2.0, M3/2.0, M4/2.0, M5/2.0, M6/2.0}, 3, 2);
		a /= 2.0;
		if(a.size() != 6)                               { err("/= test [size]");  }
		if(std::abs(ref[0]-a[0]) > 1e-15 ||
		   std::abs(ref[1]-a[1]) > 1e-15 || 
		   std::abs(ref[3]-a[3]) > 1e-15 || 
		   std::abs(ref[4]-a[4]) > 1e-15 || 
		   std::abs(ref[5]-a[5]) > 1e-15 || 
		   std::abs(ref[2]-a[2]) > 1e-15    )           { err("/= test [elements]"); }
	}

	//Test operator/ (l-value, scalar)
	{
		Matrix<double> a({M1, M2, M3, M4, M5, M6}, 3, 2);
		Matrix<double> ref({M1/2.0, M2/2.0, M3/2.0, M4/2.0, M5/2.0, M6/2.0}, 3, 2);
		Matrix<double> res = a / 2.0;
		if(a.size()   != 6)                             { err("operator/ test (l-value, scalar) [src size]");     }
		if(res.size() != 6)                             { err("operator/ test (l-value, scalar) [size]");         }
		if(a[0] != M1 || a[1] != M2 || a[2] != M3 || a[3] != M4 || a[4] != M5 || a[5] != M6 )      { err("operator/ test (l-value, scalar) [src elements]"); }
		if(std::abs(ref[0]-res[0]) > 1e-15 ||
		   std::abs(ref[1]-res[1]) > 1e-15 || 
		   std::abs(ref[3]-res[3]) > 1e-15 || 
		   std::abs(ref[4]-res[4]) > 1e-15 || 
		   std::abs(ref[5]-res[5]) > 1e-15 || 
		   std::abs(ref[2]-res[2]) > 1e-15    )         { err("operator/ test (l-value, scalar) [elements]"); }
	}

	//Test operator/ (r-value, scalar)
	{
		Matrix<double> a({M1, M2, M3, M4, M5, M6}, 3, 2);
		Matrix<double> ref({M1/2.0, M2/2.0, M3/2.0, M4/2.0, M5/2.0, M6/2.0}, 3, 2);
		Matrix<double> res = std::move(a) / 2.0;
		if(a.size()   != 0)                            { err("operator/ test (r-value, scalar) [src size]"); }
		if(res.size() != 6)                            { err("operator/ test (r-value, scalar) [size]");     }
		if(std::abs(ref[0]-res[0]) > 1e-15 ||
		   std::abs(ref[1]-res[1]) > 1e-15 || 
		   std::abs(ref[3]-res[3]) > 1e-15 || 
		   std::abs(ref[4]-res[4]) > 1e-15 || 
		   std::abs(ref[5]-res[5]) > 1e-15 || 
		   std::abs(ref[2]-res[2]) > 1e-15    )        { err("operator/ test (r-value, scalar) [elements]"); }
	}

	//Test matrix multiplication
	{
		Matrix<double> a({M1, M2, M3}, 1,3);
		Matrix<double> b({M4, M5, M6}, 3,1);
		Matrix<double> ref({M1*M4, M1*M5, M1*M6, M2*M4, M2*M5, M2*M6, M3*M4, M3*M5, M3*M6}, 3,3);
		Matrix<double> res = a * b;
		if(a.size() != 3)                             { err("multiplication test [src size]");     }
		if(b.size() != 3)                             { err("multiplication test [src size]");     }
		if(a[0] != M1 || a[1] != M2 || a[2] != M3 )    { err("multiplication test [src elements]"); }
		if(b[0] != M4 || b[1] != M5 || b[2] != M6 )    { err("multiplication test [src elements]"); }
		if(std::abs(ref[0]-res[0]) > 1e-15 ||
		   std::abs(ref[1]-res[1]) > 1e-15 || 
		   std::abs(ref[2]-res[2]) > 1e-15 ||
		   std::abs(ref[3]-res[3]) > 1e-15 ||
		   std::abs(ref[4]-res[4]) > 1e-15 ||
		   std::abs(ref[5]-res[5]) > 1e-15    )       { err("multiplication test (l-value, l-value) [elements]"); }


		// Test ostream

		cout << "Resulting matrix: " << endl <<  res << endl;
	}

    std::cout << "All tests are successful!\n";
	return 0;
}