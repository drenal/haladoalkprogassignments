#include <iostream>
#include <cmath>


template<typename State, typename T, typename E>
struct RK4Solver 
{
    // States stored as class variables, so they don't
    // need to be destroyed and reallocated in each RK4 step
    State y_;

    State k1_;
    State k2_;
    State k3_;
    State k4_;

    State k_sum_;

    State y_full_;
    State y_half_;

    State unity_;

    T t_;
    T t_end_;

    E adaptive_error_;

    T step_;

    RK4Solver(State y0, T t0, T t1, E error)
    : y_(y0), k1_(y0), k2_(y0), k3_(y0), k4_(y0), k_sum_(y0), y_full_(y0), 
      y_half_(y0), t_(t0), t_end_(t1), adaptive_error_(error)
    {
        unity_ = State([](int i, int j){return 1.0;}, y0.rows(), y0.columns() );
    }

    template<typename RHS, typename Callback>
    State & rk4solve(T h, RHS f, Callback cb)
    {
        step_ = h;
        int step_counter = 0;
        double step_full = 0.0;
        double step_half = 0.0;
        // do ODE until specified time

        // At time 0
        cb(t_, y_);

        while(t_ < t_end_)
        {
            // adjust stepsize of the last step, if it would exceed the max time
            if(t_ + step_ > t_end_)
            { 
                step_ = t_end_ - t_; 
            }

            // do a full step
            step_full = step_;
            rk4step(y_,      t_, step_full, f, y_full_);


            // do two half steps
            step_half = step_ / ((T)2);
            rk4step(y_,      t_, step_half, f, y_half_);
            rk4step(y_half_, t_, step_half, f, y_half_);

            // find max error      
            auto  delta_max = std::inner_product(y_half_.cbegin(), y_half_.cend(), y_full_.cbegin(), 0.0, 
                                                [](auto  const candidate,  auto  const curr){
                                                    if(candidate > curr) 
                                                        return candidate; 
                                                    return curr;
                                                },
                                                [](auto const two_half_step_state, auto const full_step_state){
                                                    return std::abs(two_half_step_state - full_step_state);
                                                });

            // is the error bigger than allowed?
            if(delta_max > adaptive_error_ && step_ > 1e-3)
            {
                // update step size
                step_ *= std::pow( ( adaptive_error_ / delta_max ) , 1.0/5.0);
                std::cout << "New proposed stepsize: " << step_ << " " << adaptive_error_ << " " << delta_max << std::endl;
                // and redo the steps above
            }
            else
            {
                // we accept the result of the two half steps
                y_ = y_half_;

                // enforce [-1,+1] range
                std::for_each(y_.begin(), y_.end(), [](auto &value){ if(value > 1.0) value = 1.0; if(value < -1.0) value = -1.0; });

                t_ += step_;

                if(0 == (step_counter++)%100)
                {
                    cb(t_, y_);
                }
            }
        }

        cb(t_, y_);
        return y_;
    }

    template<typename RHS>
    auto rk4step(State const & y_start, T const& t, T const& step, RHS f, State & result)
    {
        // f signature:
        // f( time, state0, state1, modifier, halfstate )
        f(t,                 y_start,                        k1_);
        f(t + step * (T)0.5, y_start + step * (T) 0.5 * k1_, k2_);
        f(t + step * (T)0.5, y_start + step * (T) 0.5 * k2_, k3_ );
        f(t + step,          y_start + step * k3_,           k4_ );

        k_sum_ = (step / (T) 6.0) * ( k1_ + k4_ + 2.0 * (k2_ +  k3_));

        result = y_ + k_sum_;
    }
};

template<typename T, typename G>
Matrix<T> operator*(Matrix<T> const& mtx, G const& scl)
{
	Matrix<T> result(mtx.rows(), mtx.columns()); 
	std::transform(mtx.cbegin(), mtx.cend(), result.begin(), [scl](T a){return a * (T)scl;});
	return result;
}

template<typename T, typename G>
Matrix<T> operator*(G const& scl, Matrix<T> const& mtx)
{
	Matrix<T> result(mtx.rows(), mtx.columns()); 
	std::transform(mtx.cbegin(), mtx.cend(), result.begin(), [scl](T a){return (T)scl * a;});
	return result;
}

template<typename T, typename G>
Matrix<T>&& operator*(Matrix<T>&& mtx, G const& scl)
{
	std::transform(mtx.cbegin(), mtx.cend(), mtx.begin(), [scl](T a){return a*(T)scl;});
	return std::move(mtx);
}

template<typename T, typename G>
Matrix<T>&& operator*(G const& scl, Matrix<T>&& mtx)
{
	std::transform(mtx.cbegin(), mtx.cend(), mtx.begin(), [scl](T a){return (T)scl*a;});
	return std::move(mtx);
}
