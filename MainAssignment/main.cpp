#include <iostream>
#include <limits>

#include "cahn_hilliard.h"


using namespace std;

int main()
{
    std::cout.precision(16);

    int N = 0;
    double t_start = 0.0;
    double t_max = 100.0;
    double adaptive_error = 1e-6;

    // length of transition regions between domains
    double gamma = 0.021;
    // diffusion coefficient
    double D = 1.0;
    // discretization
    double dx = 1.0;
    double dy = 1.0;

    double t_step_start = 0.001;

    // start function fingerprint: start(int size, double t_start, double t_max, double adaptive_error, double t_step_start, double D, double gamma, double dx, double dy)
    // Test case 1
    N=5; 
    t_max = 20.0;
    gamma = 0.021;
    D = 0.5;
    
    CahnHilliard ch;
    ch.start(N, t_start, t_max, adaptive_error, t_step_start, D, gamma, dx, dy);
    
    // Test case 1
    N=25; 
    t_max = 10.0;
    gamma = 4.2;
    D = 0.5;

    CahnHilliard ch2;
    ch2.start(N, t_start, t_max, adaptive_error, t_step_start, D, gamma, dx, dy);

    // Test case 3
    N=50;
    t_max = 10.0;
    gamma = 52;
    D = 0.5;

    CahnHilliard ch3;
    ch3.start(N, t_start, t_max, adaptive_error, t_step_start, D, gamma, dx, dy);


    // Test case 4
    N=100;
    t_max = 10.0;
    gamma = 2100;
    D = 0.5;

    CahnHilliard ch4;
    ch4.start(N, t_start, t_max, adaptive_error, t_step_start, D, gamma, dx, dy);

    return 0;
}