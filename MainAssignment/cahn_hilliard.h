#include <random>
#include <sstream>
#include <fstream>
#include <iostream>
#include <map>
#include <string>
#include <iterator>
#include <utility>

#include "../Assignment5/matrix.h"
#include "ode_solver.h"
#include "TinyPngOut.hpp"

struct CahnHilliard
{
    int start(int size, double t_start, double t_max, double adaptive_error, double t_step_start, double D, double gamma, double dx, double dy)
    {
        // initialize board as a matrix with elements of type Pixel

        std::random_device rd{};
        std::mt19937 gen(rd());
        std::normal_distribution<double> distr(0.0,1);

        Matrix<double> data(size, size);

        std::generate(data.begin(), data.end(), [&]{ return distr(gen); });

        // enforce [-1,+1] range
        std::for_each(data.begin(), data.end(), [](auto &value){ if(value > 1.0) value = 1.0; if(value < -1.0) value = -1.0; });

        //Matrix<double> data({-1.0,-1.0, 1.0,-1.0,-1.0,-1.0,-1.0,1.0,-1.0,-1.0,1.0,1.0,1.0,1.0,1.0,-1.0,-1.0,1.0,-1.0,-1.0,-1.0,-1.0,1.0,-1.0,-1.0}, size,size);

        //std::cout << data << std::endl;

        // space discretization and derivative approstate(row-1,col)mations
        // source: https://en.wikipedia.org/wiki/Finite_difference_coefficient

        auto second_order_sixth_accuracy = []( auto mthree, auto mtwo, auto mone, auto zero, auto pone, auto ptwo, auto pthree, auto denominator){
            return (     2.0 * (mthree + pthree) 
                      - 18.0 * (mtwo + ptwo)
                     + 180.0 * (mone + pone)
                     - 490.0 * zero )
                     / ( 180.0 * denominator * denominator );
        };
        // wiki: 1/90 	−3/20 	3/2 	−49/18 	3/2 	−3/20 	1/90 	
        // 1/180 * (2 - 18 + 180 -490 + 180 - 18 + 2)

        auto fourth_order_sixth_accuracy = [](auto mfour, auto mthree, auto mtwo, auto mone, auto zero, auto pone, auto ptwo, auto pthree, auto pfour, auto denominator){
            return (((     7.0 * (mfour + pfour) 
                      -   96.0 * (mthree + pthree) )
                    + (  676.0 * (mtwo + ptwo) 
                      - 1952.0 * (mone + pone) ))
                    + 2730.0 * zero ) 
                    / ( 240.0 * denominator * denominator * denominator * denominator ) 
                    ;
        };
        // wiki: 7/240 	−2/5 	169/60 	−122/15 	91/8 	−122/15 	169/60 	−2/5 	7/240 	
        // 1/240 * ( 7 - 96 + 676 -1952 + 2730 - 1952 + 676 -96 +7) * 1/d^4

        // helper lambdas

        auto cube = [](double a){return a*a*a;};

        // define Chan-Hiliard ODE system as lambda

        auto ode = [second_order_sixth_accuracy, fourth_order_sixth_accuracy, cube, D, gamma, dx, dy](double t_step, Matrix<double> const & state, Matrix<double> & result){
            for(int row = 0; row < state.rows(); ++row)
            {
                for(int col = 0; col < state.columns(); ++col)
                {
                    // dc/dt = D * (d^2/dx,y^2 c^3 - d^2/dx,y^2 c - gamma * d^4/dx,y^4 c)
                    // source: https://en.wikipedia.org/wiki/Cahn%E2%80%93Hilliard_equation

                    double laplace_c_cube = second_order_sixth_accuracy( cube(state(row-3,col)), cube(state(row-2,col)), cube(state(row-1,col)), cube(state(row,col)), cube(state(row+1,col)), cube(state(row+2,col)), cube(state(row+3,col)), dx ) + 
                                            second_order_sixth_accuracy( cube(state(row,col-3)), cube(state(row,col-2)), cube(state(row,col-1)), cube(state(row,col)), cube(state(row,col+1)), cube(state(row,col+2)), cube(state(row,col+3)), dy )  ;

                    double laplace_c = second_order_sixth_accuracy( state(row-3,col), state(row-2,col), state(row-1,col), state(row,col), state(row+1,col), state(row+2,col), state(row+3,col), dx ) + 
                                       second_order_sixth_accuracy( state(row,col-3), state(row,col-2), state(row,col-1), state(row,col), state(row,col+1), state(row,col+2), state(row,col+3), dy )  ;

                    double laplace_square_c = fourth_order_sixth_accuracy( state(row-4,col), state(row-3,col), state(row-2,col), state(row-1,col), state(row,col), state(row+1,col), state(row+2,col), state(row+3,col), state(row+4,col), dx ) + 
                                              fourth_order_sixth_accuracy( state(row,col-4), state(row,col-3), state(row,col-2), state(row,col-1), state(row,col), state(row,col+1), state(row,col+2), state(row,col+3), state(row,col+4), dy )  ;

                    // save result

                    result(row, col) =  D * ( laplace_c_cube - laplace_c - gamma * laplace_square_c);
                }
            }
        };

        auto diffusion_ode = [second_order_sixth_accuracy, fourth_order_sixth_accuracy, cube, D, gamma, dx, dy](double t_step, Matrix<double> const & state, Matrix<double> & result){
    
            for(int row = 0; row < state.rows(); ++row)
            {
                for(int col = 0; col < state.columns(); ++col)
                {
                    // dc/dt = D * d^2/dx,y^2 c
                    double laplace_c = second_order_sixth_accuracy( state(row-3,col), state(row-2,col), state(row-1,col), state(row,col), state(row+1,col), state(row+2,col), state(row+3,col), dx ) + 
                                        second_order_sixth_accuracy( state(row,col-3), state(row,col-2), state(row,col-1), state(row,col), state(row,col+1), state(row,col+2), state(row,col+3), dy )  ;
            
                    // save result
                    result(row, col) =  D * laplace_c ;
                }
            }
        };

        // keep track of approstate(row-1,col)mated cluster size
        // key is time
        // value is the average cluster size at given time
        std::map<double,double> clustersize;

        auto measure_cluster_size = [&clustersize, size](double const& t_curr, Matrix<double> const& data){
            // vector of clusters. each cluster has a vector, which contains it's elements by matrix index
            std::vector< std::vector<int> > clusters;

            // loop through the matrix, find first <0 position
            for(int i = 0; i< data.size(); ++i)
            {
                // if point is white
                if(data[i] > 0.0 ) 
                {
                    // target 
                    std::pair<int,int> coordinate_end =  data.get_position_from_index(i);
                    bool new_cluster = true;

                    std::vector<int> part_of_clusters;

                    // loop through the already found clusters
                    for( auto cluster=clusters.begin(); cluster != clusters.end(); ++cluster )
                    {
                        bool part_of_this_cluster = false;

                        // loop through the elements (matrix array ids) of the cluster
                        for( auto element = cluster->rbegin(); element != cluster->rend(); ++element )
                        {
                            std::pair<int,int> coordinate_start = data.get_position_from_index(*element);

                            int distance_x = coordinate_end.first  - coordinate_start.first;
                            int distance_y = coordinate_end.second - coordinate_start.second;

                            int x = coordinate_start.first;
                            int y = coordinate_start.second;

                            int orientation_x = (distance_x != 0) ? distance_x / std::abs(distance_x) : 0;
                            int orientation_y = (distance_y != 0) ? distance_y / std::abs(distance_y) : 0;

                            //std::cout << "Starting from x=" << x << " y=" << y << " orientations "  << orientation_x << " " << orientation_y << " distances " << distance_x << " " << distance_y << " end: " << coordinate_end.first << " " << coordinate_end.second << " index: " << i << std::endl;

                            bool y_turn = false;
                            bool swapped = false;

                            // step i in direction to the target
                            int steps = 0;
                            while( x != coordinate_end.first || y != coordinate_end.second ) 
                            {
                                if(data(x,y) < 0.0)
                                {
                                    // wrong move
                                    break;
                                }

                                // if next turn would be wrong and we didn't swap the coordinates yet
                                // swap the steps
                                if(y_turn && ! swapped && data(x,y+1) < 0.0 )
                                {
                                    y_turn = false;
                                    swapped = true;
                                }

                                if(! y_turn && ! swapped && data(x+1,y) < 0.0)
                                {
                                    y_turn = true;
                                    swapped = true;
                                }

                                if(y_turn)
                                {
                                    if(coordinate_end.second != y)
                                    {
                                        int denominator = std::abs((int)(distance_x/distance_y));
                                        if( denominator == 0 || std::abs(coordinate_start.first - x) % denominator == 0) 
                                        {
                                            y += orientation_y;
                                        }
                                    }
                                    else
                                    {
                                        if(coordinate_end.first != x)
                                        {
                                            x += orientation_x;
                                        }
                                        else
                                        {
                                            // both x and y reached end posiiton
                                            break;
                                        }
                                    }
                                    y_turn = false;
                                }
                                else
                                {
                                    if(coordinate_end.first != x)
                                    {
                                        int denominator = std::abs((int)(distance_y/distance_x));
                                        if( denominator == 0 || std::abs(coordinate_start.second - y) % denominator  == 0) 
                                        {
                                            x += orientation_x;
                                        }
                                    }
                                    else
                                    {
                                        if(coordinate_end.second != y)
                                        {
                                            y += orientation_y;
                                        }
                                        else
                                        {
                                            // both x and y reached end posiiton
                                            break;
                                        }
                                    }
                                    y_turn = true;
                                }
                                
                                swapped = false;
                                steps++;

                                // emergency stop - stop infinite loop when steps equal to 2*distance is reached
                                if(steps > (int) 2*std::sqrt(distance_x*distance_x + distance_y*distance_y))
                                {
                                    break;
                                }

                            } // while( x != coordinate_end.first || y != coordinate_end.second ) 

                            if(x == coordinate_end.first && y == coordinate_end.second )
                            {
                                part_of_this_cluster = true;
                                // no need to do more checks
                                break;
                            }
                        } // for( auto element = cluster->begin(); element != cluster->end(); ++element )
                        if(part_of_this_cluster)
                        {
                            part_of_clusters.push_back(cluster - clusters.begin());
                            new_cluster = false;
                            break;
                        }
                    } // for( auto cluster=clusters.begin(); cluster != clusters.end(); ++cluster )

                    if(new_cluster)
                    {
                        clusters.push_back(std::vector<int>{i});
                    }
                    else
                    {
                        if(part_of_clusters.size() == 1)
                        {
                            clusters.at(part_of_clusters.at(0)).push_back(i);
                        }
                        else if(part_of_clusters.size() > 1)
                        {
                            // merge two clusters
                            // don't do the merge for the first (index: 0) cluster!
                            for(auto it=part_of_clusters.rbegin(); it != part_of_clusters.rend() - 1; ++it)
                            {
                                // loop through the elements and add to the first one
                                for(auto elements : clusters.at(*it))
                                {
                                    clusters.at(part_of_clusters.at(0)).push_back(elements);
                                }
                                
                                clusters.erase(clusters.begin() + *it);
                            }

                            clusters.at(part_of_clusters.at(0)).push_back(i);
                        }
                        else
                        {
                            // error
                        }
                    } // if(new_cluster)
                }
                else
                {
                    // nothing
                }
                    
            }

            /*
            std::cout << "Found clusters: " << std::endl;
            // loop through the already found clusters
            for( auto cluster=clusters.begin(); cluster != clusters.end(); ++cluster )
            {
                std::cout << "Cluster number " << (cluster - clusters.begin())  << "elements: ";

                // loop through the elements (matrix array ids) of the cluster
                for( auto element = cluster->begin(); element != cluster->end(); ++element )
                {
                    std::cout << *element << " "; 
                }
                std::cout  << std::endl;
            }
            */

            int number_of_clusters = clusters.size();
            if(number_of_clusters == 0) number_of_clusters = 1;
            int clustersizes_sum = std::accumulate(clusters.begin(), clusters.end(), 0, [](int curr, std::vector<int> cluster){return curr + cluster.size(); } );

            clustersize.insert(std::pair<double, double>(t_curr, clustersizes_sum / number_of_clusters ) );
        };

        int step_counter = 0;

        auto callback = [measure_cluster_size, t_max,size,&step_counter](double const& t_curr, Matrix<double> const& data){
            // file name
            std::stringstream ss;
            ss << "cahn-hilliard-" << size << "-step-" << step_counter++ << "-time-" << t_curr << ".png";

            try {
                std::ofstream out(ss.str(), std::ios::binary);
                TinyPngOut pngout(static_cast<std::uint32_t>(size), static_cast<std::uint32_t>(size), out);

                for(int row = 0; row < size; ++row)
                {
                    std::vector<uint8_t> line(static_cast<size_t>(size*3) );

                    for(int col = 0; col < size; ++col)
                    {
                         
                        // Grayscale output
                        uint8_t value = (1.0 - data(row,col)) * 128.0;
                        if(value != 0)
                        {
                            value -= 1;
                        }

                        line.at(3*col) = value;
                        line.at(3*col + 1) = value;
                        line.at(3*col + 2) = value;
                        

                        // Black and white output
                        /*if(data(row,col) > 0.0)
                        {
                            // RGB black
                            line.at(3*col) = 0state(row,col)0;
                            line.at(3*col + 1) = 0state(row,col)0;
                            line.at(3*col + 2) = 0state(row,col)0;
                        }
                        else
                        {
                            // RGB white
                            line.at(3*col) = 0xFF;
                            line.at(3*col + 1) = 0xFF;
                            line.at(3*col + 2) = 0xFF;
                        }*/
                    }

                    pngout.write(line.data(), static_cast<size_t>(size));
                }
                
            } catch (const char *msg) {
                std::cerr << msg << std::endl;
                return 1;
            }

            // run cluster size measurer

            measure_cluster_size(t_curr, data);

            return 0;
        };

        //measure_cluster_size(0.0,data);
        //return 0;
        // initialie ode solver

        RK4Solver rk4solv(data, t_start, t_max, adaptive_error);

        // start rk4solver

        rk4solv.rk4solve(t_step_start, ode, callback);

        // write out clustersize data

        std::vector<std::string> results_string;
        std::transform(clustersize.cbegin(), clustersize.cend(), back_inserter(results_string), [](std::pair<double,double> const & value){std::stringstream ss; ss << value.first << " " << value.second; return ss.str();} );

        // write out to file

        // file name
        std::stringstream ss;
        ss << "cahn-hilliard-" << size << "-clustersize.txt";

        std::ofstream output(ss.str());
        if(output.is_open())
        {
            std::copy(results_string.begin(), results_string.end(), std::ostream_iterator<std::string>(output, "\n") );
        }

        return 0;
    }
};