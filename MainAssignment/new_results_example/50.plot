set title "Cahn Hilliard on a 50x50 square"
set ylabel "Average clustersize"
set xlabel "Time"


f(x) = a*x**b

fit f(x) "cahn-hilliard-50-clustersize.txt" u 1:2 via a,b

set terminal png size 800,600
set output "cahn-hilliard-50-clustersize.png"

plot "cahn-hilliard-50-clustersize.txt" u 1:2 t "{gamma}=52,D=0.5", f(x) t sprintf("f(x)=%.04f * x ** %.04f", a,b)
