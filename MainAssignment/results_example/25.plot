set title "Cahn Hilliard on a 25x25 square"
set ylabel "Average clustersize"
set xlabel "Time"

set xrange [0:5]

f(x) = a*x**b

fit f(x) "cahn-hilliard-25-clustersize.txt" u 1:2 via a,b

set terminal png size 800,600
set output "cahn-hilliard-25-clustersize.png"

plot "cahn-hilliard-25-clustersize.txt" u 1:2 t "{gamma}=4.2,D=1", f(x) t sprintf("f(x)=%.04f * x ** %.04f", a,b)
