#!/bin/bash

if [ ! -f ./times.txt ] && [ ! -f ./build/times.txt ]; then
    echo "No times.txt found, please run the benchmark first."
    exit 1
fi

if [ ! -f ./times.txt ]; then
    cp ./build/times.txt ./
fi

echo "Plotting and fitting..."
gnuplot fit_n3.gnuplot


