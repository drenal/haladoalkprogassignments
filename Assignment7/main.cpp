#include "../Assignment5/matrix.h"
#include <iostream>
#include <fstream>
#include <iterator>
#include <random>
#include <chrono>
#include <map>
#include <algorithm>
#include <sstream>

// Don't forget to build with Release mode:
// cmake -DCMAKE_BUILD_TYPE=Release ..
// make
// ./benchmark

using namespace std;

template<typename T>
vector<T> linspace(T start, T end, int elements)
{
    vector<T> result(elements);
    T step = ( end - start ) / static_cast<T>(elements-1);
    T j = start;

    generate(result.begin(), result.end(), [j,step]() mutable {j += step; return j-step;});

    return result;
}

int main()
{
    random_device rd{};
    mt19937 gen(rd());
    normal_distribution<double> distr(100,20);

    // get x axis values
    vector<double> range = linspace(10.0, 1000.0, 100);

    map<double,double> results;
    // fill out the map's keys with the x axis values, set y value to zero
    transform(range.cbegin(), range.cend(), inserter(results, results.begin()), [](double value){return pair<double, double>(value, 0.0);} );

    for(auto it = results.begin(); it != results.end(); ++it)
    {
        int loops = 100;
        vector<double> times_measured(loops);

        for(int i = 0; i < loops; ++i)
        {
            Matrix<double> mtx1(it->first, it->first);
            generate(mtx1.begin(), mtx1.end(), [&]{return distr(gen); });
            Matrix<double> mtx2(it->first, it->first);
            generate(mtx2.begin(), mtx2.end(), [&]{return distr(gen); });

            auto t1 = chrono::high_resolution_clock::now();
            Matrix<double> mtx3 = mtx1 * mtx2;
            auto t2 = chrono::high_resolution_clock::now();

            times_measured.at(i) = (static_cast<std::chrono::duration<double, std::milli>>(t2-t1)).count();
        }

        // get minimum
        it->second = *(min_element(times_measured.begin(), times_measured.end()));
    }

    // convert results map to a string vector
    vector<string> results_string;
    transform(results.cbegin(), results.cend(), back_inserter(results_string), [](pair<double,double> const & value){stringstream ss; ss << value.first << " " << value.second; return ss.str();} );

    // write out to file
    std::ofstream output("times.txt");
    if(output.is_open())
    {
        copy(results_string.begin(), results_string.end(), ostream_iterator<string>(output, "\n") );
    }

    return 0;
}