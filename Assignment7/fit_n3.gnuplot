f(x) = a*x**b
fit f(x) "times.txt" via a,b

set title "Time of square matrix multiplication as a function of matrix dimension\nMinimum time is taken from 100 measurements for each dimension"
set xlabel "Dimension of square matrix (N)"
set ylabel "Minimum time of multiplication (t) [ms]"
set key left 

set term png size 800,800
set output "times.png"

plot "times.txt" using 1:2 t "time measurements" ls 2, f(x) title sprintf("t(N)=%.10f * N^{%.10f}", a, b) ls 3