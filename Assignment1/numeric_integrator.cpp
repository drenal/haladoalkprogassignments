#include <iostream>
#include <cmath>

using namespace std;

double integrate(int n, double x0, double x1);

int main(int argc, char** argv)
{
    /*
    int n;
    double x0,x1;

    if(argc == 4)
    {
        n = stoi(argv[1]);
        x0 = stod(argv[2]);
        x1 = stod(argv[3]);
    }
    else
    {
        cout << "Please give the (initial) number of points (n): ";
        cin >> n;
        cout << "Please give the starting value (x0): ";
        cin >> x0;
        cout << "Please give the ending value (x1): ";
        cin >> x1;
    }*/

    //int n = 1;
    double x0 = -1.0;
    double x1 = 3.0;
    // specifying the exact solution very exactly although
    //  double has only 16 significant decimal positions
    double exact_solution = 1.34638795680345037669816;

    cout.precision(16);
    cout << "# n \n#\t integral's result\n#\t\t\t\t difference to exact result" << endl;

    for(int i=1; i<=100001; i+=1000)
    {
        double result = integrate(i,x0,x1);
        cout << i << "\t" << result  << "\t" << abs(exact_solution - result) << endl;
    }
}

// implementing composite Simpson's rule
// implicit assumptions:
//  * n > 0
//  * x1 > x0
double integrate(int n, double x0, double x1)
{
    if(n <= 0)
    {
        cerr << "Number of steps smaller than or equal to zero." << endl;
        return 0.0;
    }

    if(x0 > x1)
    {
        swap(x0,x1);
    }

    // integral's body as lambda
    auto body = [](double x){ return cos(x) * exp(-x*x); };

    double h = (x1 - x0)/n;
    double I0 = body(x0) + body(x1);
    double I1 = 0.0;
    double I2 = 0.0;

    for(int i=1; i<n; i++)
    {
        double x = x0 + i*h;
        if(i%2 == 0) 
        {
            I1 += body(x);
        }
        else
        {
            I2 += body(x);            
        }
    }

    return 1.0/3.0 * h * (I0 + 2* I1 + 4 * I2);
}