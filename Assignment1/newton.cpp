#include <iostream>
#include <cmath>

// always use static const instead of #define directive
static const double Precision = 1e-8;
// global variables to handle non-converging case (cap the number of iterations)
static const int MaxIterations = 20;
static int iterations = 0;

using namespace std;

double sqrt_newton_recursive(double num, double x0);

double sqrt_newton_iteration(double num, double x0);

int main(int argc, char** argv)
{
    double num = 0.0;
    double x0 = 10.0;

    cout.precision(16);

    if(argc == 2)
    {
        num = stod(argv[1]);
    }
    else
    {
        cout << "Please give a number to calculate it's square root: ";
        cin >> num;
    }

    cout << "The square root with recursive function is: " << sqrt_newton_recursive(num, x0) << endl;

    cout << "The square root with iterative function is: " << sqrt_newton_recursive(num, x0) << endl;
}

double sqrt_newton_iteration(double num, double x0)
{
    double x1=x0;
    for(int i = 0; i < MaxIterations; i++)
    {
        x1 = x1 - (x1*x1 - num) / (2*x1);
    }

    return x1;
}

double sqrt_newton_recursive(double num, double x0)
{
    if(iterations >= MaxIterations) 
    {
        return x0;
    }

    double x1 = x0 - (x0*x0 - num) / (2*x0);
    iterations++;

    // no real reason to save the difference here, but I wanted to try it out :-)
    if(double diff = abs(x0 - x1); diff < Precision)
    {
        return x1;
    }
    else
    {
        return sqrt_newton_recursive(num, x1);
    }
}
