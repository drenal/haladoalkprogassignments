#include <iostream>
#include <array>
#include <vector>
#include <numeric> //accumulate

using namespace std;

template<typename T>
array<T,3> fit_line(vector<T> const & x, vector<T> const & y);

int main()
{
	auto err = [](auto str){ std::cout << "linear_fitting.cpp error in: " << str << "\n"; std::exit(-1); };
    const double precision = 1e-6;

    // TEST#1

    vector<double> a = {0,1,2,3,4,5,6,7,8,9,10};
    vector<double> b = {0,1,2,3,4,5,6,7,8,9,10};
    // result[0] ... slope
    // result[1] ... y-intercept
    // result[2] ... r^2
    array<double, 3> result;

    result = fit_line<double>(a,b);

    cout << "Fitting line to points taken from y(x)=x function...";
    // expected result: For points of y=x, slope: 1 y-intercept: 0 r squared: 1
    if(result[0] != 1.0)                                    { err("y=x test [slope]");}
    if(result[1] != 0.0)                                    { err("y=x test [y-intercept]");}
    if(result[2] != 1.0)                                    { err("y=x test [r^2]");}

    cout << " passed." << endl;

    // TEST#2
    
    vector<double> a2 = {0,1,2,3,4,5,6,7,8,9,10};
    vector<double> b2 = {5,10,15,20,25,30,35,40,45,50,55};
    // result[0] ... slope
    // result[1] ... y-intercept
    // result[2] ... r^2
    array<double, 3> result2;

    result2 = fit_line<double>(a2,b2);

    cout << "Fitting line to points taken from y(x)=5x+5 function...";
    // expected result: For points of y=5x+5, slope: 5 y-intercept: 5 r squared: 1
    if(result2[0] != 5.0)                                    { err("y=5x+5 test [slope]");}
    if(result2[1] != 5.0)                                    { err("y=5x+5 test [y-intercept]");}
    if(result2[2] != 1.0)                                    { err("y=5x+5 test [r^2]");}

    cout << " passed." << endl;

    // TEST#3
    
    vector<double> a3 = {0,1,2,3,4,5,6,7,8,9,10};
    vector<double> b3 = {5.1,11,14,20.1,24.3,33,35,40.2,45.3,50.1,55};
    // result[0] ... slope
    // result[1] ... y-intercept
    // result[2] ... r^2
    array<double, 3> result3;

    result3 = fit_line<double>(a3,b3);

    cout << "Fitting line to points taken from y(x)=5x+5 function with noise...";
    // expected result: 
    // WolframAlpha linear fit: linear fit {0,5.1},{1,11},{2,14},{3,20.1},{4,24.3}, {5,33},  {6,35},{7,40.2}, {8,45.3},{9,50.1},{10,55}
    // Least-squares best fit: 5.00636 x + 5.25
    // AIC | BIC | R^2 | adjusted R^2
    // 37.1935 | 38.3872 | 0.996108 | 0.995676
    if(result3[0] - 5.00636  > 10.0*precision)                   { err("y=5x+5 with noise test [slope]");}
    if(result3[1] - 5.25     > precision)                        { err("y=5x+5 with noise test [y-intercept]");}
    if(result3[2] - 0.996108 > precision)                        { err("y=5x+5 with noise test [r^2]");}

    cout << " passed." << endl;

    // TEST#4
    
    vector<double> a4 = {0,1,2,3,4,5,6,7,8,9,10};
    vector<double> b4 = {5.1,11,14,20.1,24.3,33,35,40.2,45.3,50.1,90};
    // result[0] ... slope
    // result[1] ... y-intercept
    // result[2] ... r^2
    array<double, 3> result4;

    result4 = fit_line<double>(a4,b4);

    cout << "Fitting line to points taken from y(x)=5x+5 function with outlier...";
    // result: 
    // WolframAlpha linear fit: linear fit {0,5.1},{1,11},{2,14},{3,20.1},{4,24.3}, {5,33},  {6,35},{7,40.2}, {8,45.3},{9,50.1},{10,90}
    // Least-squares best fit: 6.59727 x + 0.477273
    // AIC | BIC | R^2 | adjusted R^2
    // 84.9036 | 86.0973 | 0.853156 | 0.83684
    if(result4[0] - 6.59727  > 10.0*precision)                    { err("y=5x+5 with outlier test [slope]");}
    if(result4[1] - 0.477273 > precision)                         { err("y=5x+5 with outlier test [y-intercept]");}
    if(result4[2] - 0.853156 > precision)                         { err("y=5x+5 with outlier test [r^2]");}

    cout << " passed." << endl;
}


template<typename T>
array<T,3> fit_line(vector<T> const & x, vector<T> const & y)
{
    // lambdas
    auto sq = [](T x){return x*x;};
    auto plus_sq = [sq](T base, T x){return base + sq(x);};
    auto minus_sq = [sq](T base, T x){return base - sq(x);};

    // calculate mean
    T mean_x = accumulate(x.begin(), x.end(), static_cast<T>(0.0)) / static_cast<T>(x.size());
    T mean_y = accumulate(y.begin(), y.end(), static_cast<T>(0.0)) / static_cast<T>(y.size());

    auto product = [mean_x,mean_y](T xi, T yi){return (xi - mean_x) * (yi - mean_y); };
    auto sum = [](T sum, T val){return sum + val;};

    T numerator = inner_product(x.begin(), x.end(), y.begin(), static_cast<T>(0.0), sum, product);

    T denominator = accumulate(x.begin(), x.end(), static_cast<T>(0.0), [mean_x,plus_sq](T sum, T xi){return plus_sq(sum, xi-mean_x);});

    T slope = numerator / denominator;

    // mean(xy)
    auto product2 = [](T xi, T yi){return (xi * yi); };
    T mean_xy = inner_product(x.begin(), x.end(), y.begin(), static_cast<T>(0.0), sum, product2) / static_cast<T>(x.size());

    // - mean(x^2), mean(y^2)
    // if you forget the template parameter <T> after the plus_sq expect following error:
    // error: no matching function for call to ‘accumulate(std::vector<double>::const_iterator, std::vector<double>::const_iterator, double, <unresolved overloaded function type>)’
    T mean_x_squared = accumulate(x.begin(), x.end(), static_cast<T>(0.0), plus_sq) / static_cast<T>(x.size());
    T mean_y_squared = accumulate(y.begin(), y.end(), static_cast<T>(0.0), plus_sq) / static_cast<T>(y.size());

    // r squared
    T r_squared = sq( mean_xy - mean_x * mean_y ) / ( minus_sq(mean_x_squared,mean_x) * minus_sq(mean_y_squared,mean_y) );

    // return with an array of 3 elements:
    // index 0: slope
    // index 1: y-intercept
    // index 2: r^2
    return { slope, mean_y - slope * mean_x, r_squared};
}