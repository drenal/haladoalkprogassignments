#include <iostream>
#include <cmath>

using namespace std;

template<typename T, typename F, typename G, typename H>
H newton_method(T func, F deriv, G stop, H x0);

int main(int argc, char** argv)
{
    double x0 = 10.0;

    auto func = [](double x){return x*x - 612.0;};
    auto deriv = [](double x){return 2*x;};
    auto stop = [](int counter, double old_value, double new_value){
        if(counter > 20)
        {
            return true;
        }
        if(abs(new_value - old_value) < 1e-8)
        {
            return true;
        }
        return false;
    };

    cout.precision(16);
    // expected: 24.738 633 753 7
    cout << "The result of the Newton method for f(x) = x^2 - 612: " << newton_method(func, deriv, stop, x0) << endl;
    
    x0=0.5;
    auto func2 = [](double x){return cos(x) -  x*x*x; };
    auto deriv2 = [](double x){return - sin(x) - 3*x*x; };
    // expected: 0.865 474 033 102
    cout << "The result of the Newton method for f(x) = cos(x) - x^3 starting from 0.5: " << newton_method(func2, deriv2, stop, x0) << endl;
    x0=0.0001;
    // expected: should not find the solution in 20 steps
    cout << "The result of the Newton method for f(x) = cos(x) - x^3 starting from 0: " << newton_method(func2, deriv2, stop, x0) << endl;

    // to test the possibilities provided by the template
    int x0_new = 5;

    auto func3 = [](int x){return x*x - 25; };
    auto deriv3 = [](int x){return 2*x; };
    auto stop2 = [](int counter, int old_value, int new_value){
        if(counter > 20)
        {
            return true;
        }
        if(new_value == old_value)
        {
            return true;
        }
        return false;
    };

    // expected: 5
    cout << "The result of the Newton method for f(x) = x^2 - 25 starting from 5: " << newton_method(func3, deriv3, stop2, x0_new) << endl;

    x0_new = 5;

    auto func4 = [](int x){return x*x - 13; };
    auto deriv4 = [](int x){return 2*x; };

    // expected: 4
    cout << "The result of the Newton method for f(x) = x^2 - 13 starting from 5: " << newton_method(func4, deriv4, stop2, x0_new) << endl;
}


template<typename T, typename F, typename G, typename H>
H newton_method(T func, F deriv, G stop, H x0)
{
    H x1=x0;
    int counter = 0;
    do
    {
        x0 = x1;
        x1 = x1 - func(x1) / deriv(x1);
        counter++;
    }while( !stop(counter,x0,x1) );

    return x1;
}