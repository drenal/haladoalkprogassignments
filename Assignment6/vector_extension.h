#ifndef ASSIGNMENT6
#define ASSIGNMENT6
#endif

#include "../Assignment4/vector2.h"
#include <iostream>
#include <string>
#include <sstream>

template<typename G>
std::ostream& operator<<(std::ostream & o, Vector2d<G> const & v)
{
    o << v.x << ", " << v.y;
    return o;
}

template<typename G>
std::istream& operator>>(std::istream & i, Vector2d<G> & v)
{
    const auto state = i.rdstate();
    const auto pos = i.tellg();

    bool error = false;
    std::string tmp;
    std::getline(i, tmp);

    Vector2d<G> v_temp;

    if(tmp.size() > 0) 
    {
        std::stringstream ss(tmp);
        std::getline(ss, tmp, ',');
        std::stringstream ss_temp(tmp);
        ss_temp >> v_temp.x;
        std::getline(ss, tmp);
        std::stringstream ss_temp2(tmp);
        ss_temp2 >> v_temp.y;
    }

    if(error)
    {
        i.clear();
        i.seekg(pos);
        i.setstate(state);
    }
    else
    {
        v = v_temp;
    }

    return i;
}