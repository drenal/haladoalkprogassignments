#ifndef ASSIGNMENT6
#define ASSIGNMENT6
#endif

#include "../Assignment5/matrix.h"
#include <iostream>
#include <string>
#include <sstream>

// ostream <<
template<typename G>
std::ostream& operator<< (std::ostream & o, Matrix<G> const & mtx)
{
    o << mtx.rows() << ", " << mtx.columns() << std::endl;

	for(int i=0; i<mtx.rows(); ++i)
	{
		for(int j=0; j<mtx.columns(); ++j)
		{
			o << mtx(i,j) << "   ";
		}
		o << std::endl;
	}
	return o;
}

// istream >>
template<typename G>
std::istream& operator>>(std::istream & i, Matrix<G> & mtx)
{
    const auto state = i.rdstate();
    const auto pos = i.tellg();

    bool error = false;
    int M,N = 0;
    int index = 0;
    int line_counter = 0;

    std::string line;
    std::getline(i, line);
    
    std::string tmp;
    std::stringstream ss(line);

    std::getline(ss, tmp, ',');
    std::stringstream ss_temp(tmp);
    ss_temp >> M;
    std::getline(ss, tmp);
    std::stringstream ss_temp2(tmp);
    ss_temp2 >> N;

    Matrix<G> mtx_temp(M,N);

    // loop through the remaining lines
    while(std::getline(i, line))
    {
        std::stringstream sss(line);
        G temp;
        while(sss >> temp)
        {
            mtx_temp[index++] = temp;
        }
        ++line_counter;

        if(line_counter*N > index)
        {
            error = true;
            std::cerr << "More data " << index << " instead of " << line_counter * N  << " was read in than expected, aborting..." << std::endl;
            break;
        }
        if(line_counter*N < index)
        {
            error = true;
            std::cerr << "Less data " << index << " instead of " << line_counter * N  << " was read in than expected, aborting..." << std::endl;
            break;
        }
    }

    if(error)
    {
        i.clear();
        i.seekg(pos);
        i.setstate(state);
    }
    else
    {
        // set dimension and resize data container
        mtx.set_dimension(M,N);

        mtx = mtx_temp;
    }

    return i;
}