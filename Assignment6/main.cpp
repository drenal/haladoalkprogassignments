
#include "vector_extension.h"
#include "matrix_extension.h"
#include<sstream>

using namespace std;

int main()
{
	auto err = [](auto str){ std::cout << "matrix.h error in: " << str << "\n"; std::exit(-1); };

    // Vector test#1
    {
        std::stringstream ss;
        std::stringstream ss_result("3.5, 3.4");
        // print out vector
        Vector2d<double> v1{3.5,3.4};
        ss << v1;

        if(ss.str() != ss_result.str())   {err("vector output does not match expected");}

        // read in vector
        Vector2d<double> v2;
        ss_result >> v2;

        // compare vectors
        if(v1 != v2)                    {err("vector read in error");}
    }

    // Matrix test#1
    {
        std::stringstream ss;
        std::stringstream ss_result("2, 2\n1.3   4.5   \n6.7   8.7   \n");

        Matrix<double> m1({1.3,4.5,6.7,8.7}, 2, 2);
        ss << m1;

        //std::cout << ss.str() << std::endl;

        if(ss.str() != ss_result.str())   {err("matrix output does not match expected");}

        Matrix<double> m2;
        ss_result >> m2;

        if(m1 != m2)                    {err("matrix read in error");}
    }

    // Vector test#2 (should fail)
    {
        std::stringstream ss("3.5, abcd");
        Vector2d<double> v1;

        ss >> v1;

        // should not have anything set in v1
        if(v1.x != 0.0 && v1.y != 0.0 && v1.length() != 0)    {err("vector fail test failed.");}
    }


    // Matrix test#2 (should fail)
    {
        std::stringstream ss("2, 2\n1.3 4.5\nquiijfi 8.7");
        Matrix<double> mtx;

        ss >> mtx;

        // should not have anything set in v1
        if(mtx.rows() != 0.0 && mtx.columns() != 0.0 && mtx.size() != 0)    {err("matrix fail test failed.");}
    }

    std::cout << std::endl << "All tests passed..." << std::endl;
    return 0;
}